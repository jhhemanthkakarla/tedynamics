package com.viddmonk.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QUserMedia is a Querydsl query type for UserMedia
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QUserMedia extends EntityPathBase<UserMedia> {

    private static final long serialVersionUID = -154222978;

    private static final PathInits INITS = PathInits.DIRECT;

    public static final QUserMedia userMedia1 = new QUserMedia("userMedia1");

    public final StringPath id = createString("id");

    public final QUser user;

    public final QUserMedia userMedia;

    public QUserMedia(String variable) {
        this(UserMedia.class, forVariable(variable), INITS);
    }

    @SuppressWarnings("all")
    public QUserMedia(Path<? extends UserMedia> path) {
        this((Class)path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QUserMedia(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QUserMedia(PathMetadata<?> metadata, PathInits inits) {
        this(UserMedia.class, metadata, inits);
    }

    public QUserMedia(Class<? extends UserMedia> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user"), inits.get("user")) : null;
        this.userMedia = inits.isInitialized("userMedia") ? new QUserMedia(forProperty("userMedia"), inits.get("userMedia")) : null;
    }

}

