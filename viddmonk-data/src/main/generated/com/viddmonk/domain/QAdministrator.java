package com.viddmonk.domain;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QAdministrator is a Querydsl query type for Administrator
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAdministrator extends EntityPathBase<Administrator> {

    private static final long serialVersionUID = 1087499250;

    private static final PathInits INITS = PathInits.DIRECT;

    public static final QAdministrator administrator = new QAdministrator("administrator");

    public final EnumPath<AccountState> accountState = createEnum("accountState", AccountState.class);

    public final StringPath email = createString("email");

    public final StringPath id = createString("id");

    public final StringPath name = createString("name");

    public final NumberPath<Integer> numberOfFailedAttempts = createNumber("numberOfFailedAttempts", Integer.class);

    public final StringPath password = createString("password");

    public final QAdministratorRole role;

    public final StringPath username = createString("username");

    public final StringPath zipCode = createString("zipCode");

    public QAdministrator(String variable) {
        this(Administrator.class, forVariable(variable), INITS);
    }

    @SuppressWarnings("all")
    public QAdministrator(Path<? extends Administrator> path) {
        this((Class)path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QAdministrator(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QAdministrator(PathMetadata<?> metadata, PathInits inits) {
        this(Administrator.class, metadata, inits);
    }

    public QAdministrator(Class<? extends Administrator> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.role = inits.isInitialized("role") ? new QAdministratorRole(forProperty("role")) : null;
    }

}

