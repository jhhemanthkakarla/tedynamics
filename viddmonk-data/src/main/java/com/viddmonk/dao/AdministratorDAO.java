package com.viddmonk.dao;

import com.viddmonk.domain.Administrator;

/**
 * Administrator DAO Interface
 *
 */
public interface AdministratorDAO {
	
	public Administrator findByUsername(String username);

}