package com.viddmonk.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.QAdministrator;
import com.viddmonk.repository.AdministratorRepository;
import com.mysema.query.types.expr.BooleanExpression;

/**
 * Administrator DAO Interface
 *
 */
@Repository
public class AdministratorDAOImpl implements AdministratorDAO {
        
	@Autowired
	private AdministratorRepository administratorRepository;
	
	/**
	 * find by username
	 * @param	username	Username
	 * @return	Administrator
	 * 
	 */
	public Administrator findByUsername(String username) {
		QAdministrator administrator = QAdministrator.administrator;
		BooleanExpression expression = administrator.username.eq(username);
		return administratorRepository.findOne(expression);
	}
}