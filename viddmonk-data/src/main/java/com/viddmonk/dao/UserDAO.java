package com.viddmonk.dao;

import com.viddmonk.domain.User;

/**
 * User DAO Interface
 *
 */
/** testing comment*/

public interface UserDAO {
	
	public User findByUsername(String username);

}