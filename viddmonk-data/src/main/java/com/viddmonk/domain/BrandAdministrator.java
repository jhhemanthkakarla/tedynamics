package com.viddmonk.domain;

import java.io.Serializable;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "branduser")
public class BrandAdministrator implements Serializable  {

	private static final long serialVersionUID = 7764852955422883687L;

	@Id
	private String id;

	@DBRef
	private Brand brand;

	@DBRef
	private Administrator administrator;

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get brand
	 * @return the brand
	 *
	 */
	public Brand getBrand() {
		return brand;
	}

	/**
	 * set brand
	 * @param brand the brand to set
	 *
	 */
	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	/**
	 * get administrator
	 * @return the administrator
	 *
	 */
	public Administrator getAdministrator() {
		return administrator;
	}

	/**
	 * set administrator
	 * @param administrator the administrator to set
	 *
	 */
	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BrandAdministrator other = (BrandAdministrator) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}