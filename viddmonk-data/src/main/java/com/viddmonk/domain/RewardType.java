package com.viddmonk.domain;

/**
 * Reward Type enum
 * @author rvadlam
 *
 */
public enum RewardType {
	 Cash
	,Reward
	,Coupon
	,Item
	,Merchandise
}
