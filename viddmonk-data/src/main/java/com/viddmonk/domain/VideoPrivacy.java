package com.viddmonk.domain;

/**
 * Video Privacy enum
 * @author rvadlam
 *
 */
public enum VideoPrivacy {
	 PRIVATE
	,PUBLIC
}
