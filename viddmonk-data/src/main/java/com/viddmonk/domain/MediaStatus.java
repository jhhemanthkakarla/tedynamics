package com.viddmonk.domain;

/**
 * Account State enum
 * @author rvadlam
 *
 */
public enum MediaStatus {
	 APPROVED
	,REJECTED
	,LIKED
}
