/**
 * Copyright ViddMonk 2013
 * 
 */
package com.viddmonk.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Media domain class which represents Media Table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "media")
public class Media implements Serializable {

	private static final long serialVersionUID = 5378551066986616209L;

	@Id
	private String id;
	
	private MediaType mediaType;
	
	private MediaStatus mediaStatus;
	
	@Size(max=256)	
	private String filename;
	
	@Size(max=256)	
	private String s3URL;
	
	private Long length;
	
	private Long fileSize;

    private Date uploadedTime = new Date();

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get mediaType
	 * @return the mediaType
	 *
	 */
	public MediaType getMediaType() {
		return mediaType;
	}

	/**
	 * set mediaType
	 * @param mediaType the mediaType to set
	 *
	 */
	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 * get filename
	 * @return the filename
	 *
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * set filename
	 * @param filename the filename to set
	 *
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * get s3URL
	 * @return the s3URL
	 *
	 */
	public String getS3URL() {
		return s3URL;
	}

	/**
	 * set s3URL
	 * @param s3url the s3URL to set
	 *
	 */
	public void setS3URL(String s3url) {
		s3URL = s3url;
	}

	/**
	 * get length
	 * @return the length
	 *
	 */
	public Long getLength() {
		return length;
	}

	/**
	 * set length
	 * @param length the length to set
	 *
	 */
	public void setLength(Long length) {
		this.length = length;
	}

	/**
	 * get fileSize
	 * @return the fileSize
	 *
	 */
	public Long getFileSize() {
		return fileSize;
	}

	/**
	 * set fileSize
	 * @param fileSize the fileSize to set
	 *
	 */
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * get uploadedTime
	 * @return the uploadedTime
	 *
	 */
	public Date getUploadedTime() {
		return uploadedTime;
	}

	/**
	 * set uploadedTime
	 * @param uploadedTime the uploadedTime to set
	 *
	 */
	public void setUploadedTime(Date uploadedTime) {
		this.uploadedTime = uploadedTime;
	}

	/**
	 * get mediaStatus
	 * @return the mediaStatus
	 *
	 */
	public MediaStatus getMediaStatus() {
		return mediaStatus;
	}

	/**
	 * set mediaStatus
	 * @param mediaStatus the mediaStatus to set
	 *
	 */
	public void setMediaStatus(MediaStatus mediaStatus) {
		this.mediaStatus = mediaStatus;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fileSize == null) ? 0 : fileSize.hashCode());
		result = prime * result
				+ ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((length == null) ? 0 : length.hashCode());
		result = prime * result
				+ ((mediaType == null) ? 0 : mediaType.hashCode());
		result = prime * result + ((s3URL == null) ? 0 : s3URL.hashCode());
		result = prime * result
				+ ((uploadedTime == null) ? 0 : uploadedTime.hashCode());
		return result;
	}
}