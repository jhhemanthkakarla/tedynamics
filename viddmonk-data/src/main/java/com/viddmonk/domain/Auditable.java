package com.viddmonk.domain;

import java.util.Date;

/**
 * Auditable Entity
 *
 */
public class Auditable {

    private Date createDate = new Date();

    private Date updateDate;

    private String createUser;

    private String updateUser;

	/**
	 * get createDate
	 * @return the createDate
	 *
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * set createDate
	 * @param createDate the createDate to set
	 *
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * get updateDate
	 * @return the updateDate
	 *
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * set updateDate
	 * @param updateDate the updateDate to set
	 *
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * get createUser
	 * @return the createUser
	 *
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * set createUser
	 * @param createUser the createUser to set
	 *
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * get updateUser
	 * @return the updateUser
	 *
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * set updateUser
	 * @param updateUser the updateUser to set
	 *
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}