package com.viddmonk.domain;

import java.io.Serializable;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "usermedia")
public class UserMedia implements Serializable  {

	private static final long serialVersionUID = -8568654378391749282L;

	@Id
	private String id;

	@DBRef
	private User user;

	@DBRef
	private UserMedia userMedia;

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get user
	 * @return the user
	 *
	 */
	public User getUser() {
		return user;
	}

	/**
	 * set user
	 * @param user the user to set
	 *
	 */
	public void setUser(User user) {
		this.user = user;
	}
}