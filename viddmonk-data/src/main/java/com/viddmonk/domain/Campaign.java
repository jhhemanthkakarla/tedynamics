package com.viddmonk.domain;

/**
 * Copyright Viddmonk 2013
 * 
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Brand domain class which represents Brand Table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "campaigns")
public class Campaign extends Auditable implements Serializable {

	private static final long serialVersionUID = -1561673286530058824L;

	@Id
	private String id;
	
	@NotNull
	@NotEmpty
	@Size(min=1, max=128)	
	private String title;
	
	@Size(max=256)	
	private String tagline;	
	
	@Size(max=256)	
	private String shortDescrption;	
		
	@Size(max=2000)	
    private String description;	
	
	@Size(max=256)	
	private String instructions;	
	
	@Size(max=256)	
	private String creativeVideoLength;	
	
	@Size(max=256)	
	private String tags;	
	
	private boolean competitive;
	
	private Double competitive1st;
	
	private boolean competitive1stPaid;
	
	private Double competitive2nd;
	
	private boolean competitive2ndPaid;
	
	private Double competitive3rd;
	
	private boolean competitive3rdPaid;
	
	private boolean performance;
	
	private Double awardByViews;
	
	private boolean awardByViewsPaid;
	
	private Double awardByLikes;
	
	private boolean awardByLikesPaid;
	
	private boolean fixedPrize;
	
	private Double fixedPrizeAmount;
	
	private boolean fixedPrizeAmountPaid;
	
	private Double totalRewardAmount = new Double(0.0);
	
	private Integer fixedPrizeQuantity;
	
	private Integer fixedPrizeQuantityPaid;
	
	private boolean Paid;
	
	private boolean smallReward;
	
	private boolean guaranteeRewards;

    private Date smallRewardExpiryStartDateTime = new Date();

    private Date smallRewardExpiryEndDateTime = new Date();
	
	@Size(max=512)	
	private String smallRewardDescription;	

	@Transient
	private String x1;
	
	@Transient 
	private String x2;
	
	@Transient 
	private String y1;
	
	@Transient
	private String y2;
	
	@Transient 
	private String w;
	
	@Transient 
	private String h;
	
	@DBRef
	private List<Media> campaignImageList = new ArrayList<Media>();
	
	@DBRef
	private List<Media> campaignVideoList = new ArrayList<Media>();
	
	@DBRef
	private List<Media> campaignAudioList = new ArrayList<Media>();
	
	@DBRef
	private Set<Media> creativeImageList = new HashSet<Media>();
	
	@DBRef
	private Set<Media> creativeAudioList = new HashSet<Media>();
	
	@DBRef
	private Set<Media> creativeVideoList = new HashSet<Media>();

    private Date startDateTime = new Date();

    private Date endDateTime = new Date();
	
	private CampaignStatus campaignStatus;
	
	private int numberOfViews;

	@Transient
    private long numberOfDaysActive;

	@Transient
    private long daysLeft;

	@Transient
    private long hoursLeft;

	@Transient
    private long minutesLeft;

	@Transient
	private Double monkPay;

	@Transient
	private Double platformFee;

	@Transient
	private Double totalCharges;
	
	@DBRef
	private Set<Tag> tagList = new HashSet<Tag>();
	
	@DBRef
	private Set<Reward> rewardList = new HashSet<Reward>();
	
	@DBRef
	private Brand brand;

	@DBRef
	private List<PaymentCard> paymentCardList = new ArrayList<PaymentCard>();

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get title
	 * @return the title
	 *
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * set title
	 * @param title the title to set
	 *
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * get tagline
	 * @return the tagline
	 *
	 */
	public String getTagline() {
		return tagline;
	}

	/**
	 * set tagline
	 * @param tagline the tagline to set
	 *
	 */
	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	/**
	 * get shortDescrption
	 * @return the shortDescrption
	 *
	 */
	public String getShortDescrption() {
		return shortDescrption;
	}

	/**
	 * set shortDescrption
	 * @param shortDescrption the shortDescrption to set
	 *
	 */
	public void setShortDescrption(String shortDescrption) {
		this.shortDescrption = shortDescrption;
	}

	/**
	 * get description
	 * @return the description
	 *
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * set description
	 * @param description the description to set
	 *
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * get instructions
	 * @return the instructions
	 *
	 */
	public String getInstructions() {
		return instructions;
	}

	/**
	 * set instructions
	 * @param instructions the instructions to set
	 *
	 */
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	/**
	 * get creativeImageList
	 * @return the creativeImageList
	 *
	 */
	public Set<Media> getCreativeImageList() {
		return creativeImageList;
	}

	/**
	 * set creativeImageList
	 * @param creativeImageList the creativeImageList to set
	 *
	 */
	public void setCreativeImageList(Set<Media> creativeImageList) {
		this.creativeImageList = creativeImageList;
	}

	/**
	 * get creativeAudioList
	 * @return the creativeAudioList
	 *
	 */
	public Set<Media> getCreativeAudioList() {
		return creativeAudioList;
	}

	/**
	 * set creativeAudioList
	 * @param creativeAudioList the creativeAudioList to set
	 *
	 */
	public void setCreativeAudioList(Set<Media> creativeAudioList) {
		this.creativeAudioList = creativeAudioList;
	}

	/**
	 * get creativeVideoList
	 * @return the creativeVideoList
	 *
	 */
	public Set<Media> getCreativeVideoList() {
		return creativeVideoList;
	}

	/**
	 * set creativeVideoList
	 * @param creativeVideoList the creativeVideoList to set
	 *
	 */
	public void setCreativeVideoList(Set<Media> creativeVideoList) {
		this.creativeVideoList = creativeVideoList;
	}

	/**
	 * get startDateTime
	 * @return the startDateTime
	 *
	 */
	public Date getStartDateTime() {
		return startDateTime;
	}

	/**
	 * set startDateTime
	 * @param startDateTime the startDateTime to set
	 *
	 */
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	/**
	 * get endDateTime
	 * @return the endDateTime
	 *
	 */
	public Date getEndDateTime() {
		return endDateTime;
	}

	/**
	 * set endDateTime
	 * @param endDateTime the endDateTime to set
	 *
	 */
	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	/**
	 * get campaignStatus
	 * @return the campaignStatus
	 *
	 */
	public CampaignStatus getCampaignStatus() {
		return campaignStatus;
	}

	/**
	 * set campaignStatus
	 * @param campaignStatus the campaignStatus to set
	 *
	 */
	public void setCampaignStatus(CampaignStatus campaignStatus) {
		this.campaignStatus = campaignStatus;
	}

	/**
	 * get tagList
	 * @return the tagList
	 *
	 */
	public Set<Tag> getTagList() {
		return tagList;
	}

	/**
	 * set tagList
	 * @param tagList the tagList to set
	 *
	 */
	public void setTagList(Set<Tag> tagList) {
		this.tagList = tagList;
	}

	/**
	 * get rewardList
	 * @return the rewardList
	 *
	 */
	public Set<Reward> getRewardList() {
		return rewardList;
	}

	/**
	 * set rewardList
	 * @param rewardList the rewardList to set
	 *
	 */
	public void setRewardList(Set<Reward> rewardList) {
		this.rewardList = rewardList;
	}

	/**
	 * get brand
	 * @return the brand
	 *
	 */
	public Brand getBrand() {
		return brand;
	}

	/**
	 * set brand
	 * @param brand the brand to set
	 *
	 */
	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	/**
	 * get campaignImageList
	 * @return the campaignImageList
	 *
	 */
	public List<Media> getCampaignImageList() {
		if( campaignImageList == null ) {
			campaignImageList = new ArrayList<Media>();
		}
		return campaignImageList;
	}

	/**
	 * set campaignImageList
	 * @param campaignImageList the campaignImageList to set
	 *
	 */
	public void setCampaignImageList(List<Media> campaignImageList) {
		this.campaignImageList = campaignImageList;
	}

	/**
	 * get campaignVideoList
	 * @return the campaignVideoList
	 *
	 */
	public List<Media> getCampaignVideoList() {
		if( campaignVideoList == null ) {
			campaignVideoList = new ArrayList<Media>();
		}
		return campaignVideoList;
	}

	/**
	 * set campaignVideoList
	 * @param campaignVideoList the campaignVideoList to set
	 *
	 */
	public void setCampaignVideoList(List<Media> campaignVideoList) {
		this.campaignVideoList = campaignVideoList;
	}

	/**
	 * get campaignAudioList
	 * @return the campaignAudioList
	 *
	 */
	public List<Media> getCampaignAudioList() {
		if( campaignAudioList == null ) {
			campaignAudioList = new ArrayList<Media>();
		}
		return campaignAudioList;
	}

	/**
	 * set campaignAudioList
	 * @param campaignAudioList the campaignAudioList to set
	 *
	 */
	public void setCampaignAudioList(List<Media> campaignAudioList) {
		this.campaignAudioList = campaignAudioList;
	}

	
	/**
	 * get numberOfDaysActive
	 * @return the numberOfDaysActive
	 *
	 */
	public long getNumberOfDaysActive() {
		if( super.getCreateDate() != null ) {
			long diff = Calendar.getInstance().getTime().getTime() - super.getCreateDate().getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			return diffDays;		
		}
		return numberOfDaysActive;
	}

	/**
	 * get daysLeft
	 * @return the daysLeft
	 *
	 */
	public long getDaysLeft() {
		if( this.getEndDateTime() != null ) {
			long diff = this.getEndDateTime().getTime() - Calendar.getInstance().getTime().getTime();
			long diffDays = diff / (24 * 60 * 60 * 1000);
			if( diffDays > 0 ) {
				return diffDays;		
			}
		}
		return 0;
	}

	/**
	 * set daysLeft
	 * @param daysLeft the daysLeft to set
	 *
	 */
	public void setDaysLeft(long daysLeft) {
		this.daysLeft = daysLeft;
	}

	/**
	 * get hoursLeft
	 * @return the hoursLeft
	 *
	 */
	public long getHoursLeft() {
		if( this.getEndDateTime() != null ) {
			long diff = this.getEndDateTime().getTime() - Calendar.getInstance().getTime().getTime();
			long diffHours = diff / (60 * 60 * 1000) % 24;
			if( diffHours > 0 ) {
				return diffHours;		
			}
		}
		return 0;
	}

	/**
	 * set hoursLeft
	 * @param hoursLeft the hoursLeft to set
	 *
	 */
	public void setHoursLeft(long hoursLeft) {
		this.hoursLeft = hoursLeft;
	}

	/**
	 * get minutesLeft
	 * @return the minutesLeft
	 *
	 */
	public long getMinutesLeft() {
		if( this.getEndDateTime() != null ) {
			long diff = this.getEndDateTime().getTime() - Calendar.getInstance().getTime().getTime();
			long diffMins = diff / (60 * 1000) % 60;
			if( diffMins > 0 ) {
				return diffMins;		
			}
		}
		return 0;
	}

	/**
	 * set minutesLeft
	 * @param minutesLeft the minutesLeft to set
	 *
	 */
	public void setMinutesLeft(long minutesLeft) {
		this.minutesLeft = minutesLeft;
	}

	/**
	 * set numberOfDaysActive
	 * @param numberOfDaysActive the numberOfDaysActive to set
	 *
	 */
	public void setNumberOfDaysActive(long numberOfDaysActive) {
		this.numberOfDaysActive = numberOfDaysActive;
	}

	/**
	 * get paymentCardList
	 * @return the paymentCardList
	 *
	 */
	public List<PaymentCard> getPaymentCardList() {
		if( paymentCardList == null ) {
			paymentCardList = new ArrayList<PaymentCard>();
		}
		return paymentCardList;
	}

	/**
	 * set paymentCardList
	 * @param paymentCardList the paymentCardList to set
	 *
	 */
	public void setPaymentCardList(List<PaymentCard> paymentCardList) {
		this.paymentCardList = paymentCardList;
	}

	/**
	 * get creativeVideoLength
	 * @return the creativeVideoLength
	 *
	 */
	public String getCreativeVideoLength() {
		return creativeVideoLength;
	}

	/**
	 * set creativeVideoLength
	 * @param creativeVideoLength the creativeVideoLength to set
	 *
	 */
	public void setCreativeVideoLength(String creativeVideoLength) {
		this.creativeVideoLength = creativeVideoLength;
	}

	/**
	 * get tags
	 * @return the tags
	 *
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * set tags
	 * @param tags the tags to set
	 *
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * get competitive
	 * @return the competitive
	 *
	 */
	public boolean isCompetitive() {
		return competitive;
	}

	/**
	 * set competitive
	 * @param competitive the competitive to set
	 *
	 */
	public void setCompetitive(boolean competitive) {
		this.competitive = competitive;
	}

	/**
	 * get competitive1st
	 * @return the competitive1st
	 *
	 */
	public Double getCompetitive1st() {
		return competitive1st;
	}

	/**
	 * set competitive1st
	 * @param competitive1st the competitive1st to set
	 *
	 */
	public void setCompetitive1st(Double competitive1st) {
		this.competitive1st = competitive1st;
	}

	/**
	 * get competitive2nd
	 * @return the competitive2nd
	 *
	 */
	public Double getCompetitive2nd() {
		return competitive2nd;
	}

	/**
	 * set competitive2nd
	 * @param competitive2nd the competitive2nd to set
	 *
	 */
	public void setCompetitive2nd(Double competitive2nd) {
		this.competitive2nd = competitive2nd;
	}

	/**
	 * get competitive3rd
	 * @return the competitive3rd
	 *
	 */
	public Double getCompetitive3rd() {
		return competitive3rd;
	}

	/**
	 * set competitive3rd
	 * @param competitive3rd the competitive3rd to set
	 *
	 */
	public void setCompetitive3rd(Double competitive3rd) {
		this.competitive3rd = competitive3rd;
	}

	/**
	 * get performance
	 * @return the performance
	 *
	 */
	public boolean isPerformance() {
		return performance;
	}

	/**
	 * set performance
	 * @param performance the performance to set
	 *
	 */
	public void setPerformance(boolean performance) {
		this.performance = performance;
	}

	/**
	 * get awardByViews
	 * @return the awardByViews
	 *
	 */
	public Double getAwardByViews() {
		return awardByViews;
	}

	/**
	 * set awardByViews
	 * @param awardByViews the awardByViews to set
	 *
	 */
	public void setAwardByViews(Double awardByViews) {
		this.awardByViews = awardByViews;
	}

	/**
	 * get awardByLikes
	 * @return the awardByLikes
	 *
	 */
	public Double getAwardByLikes() {
		return awardByLikes;
	}

	/**
	 * set awardByLikes
	 * @param awardByLikes the awardByLikes to set
	 *
	 */
	public void setAwardByLikes(Double awardByLikes) {
		this.awardByLikes = awardByLikes;
	}

	/**
	 * get fixedPrize
	 * @return the fixedPrize
	 *
	 */
	public boolean isFixedPrize() {
		return fixedPrize;
	}

	/**
	 * set fixedPrize
	 * @param fixedPrize the fixedPrize to set
	 *
	 */
	public void setFixedPrize(boolean fixedPrize) {
		this.fixedPrize = fixedPrize;
	}

	/**
	 * get fixedPrizeAmount
	 * @return the fixedPrizeAmount
	 *
	 */
	public Double getFixedPrizeAmount() {
		return fixedPrizeAmount;
	}

	/**
	 * set fixedPrizeAmount
	 * @param fixedPrizeAmount the fixedPrizeAmount to set
	 *
	 */
	public void setFixedPrizeAmount(Double fixedPrizeAmount) {
		this.fixedPrizeAmount = fixedPrizeAmount;
	}

	/**
	 * get fixedPrizeQuantity
	 * @return the fixedPrizeQuantity
	 *
	 */
	public Integer getFixedPrizeQuantity() {
		return fixedPrizeQuantity;
	}

	/**
	 * set fixedPrizeQuantity
	 * @param fixedPrizeQuantity the fixedPrizeQuantity to set
	 *
	 */
	public void setFixedPrizeQuantity(Integer fixedPrizeQuantity) {
		this.fixedPrizeQuantity = fixedPrizeQuantity;
	}

	/**
	 * get smallReward
	 * @return the smallReward
	 *
	 */
	public boolean isSmallReward() {
		return smallReward;
	}

	/**
	 * set smallReward
	 * @param smallReward the smallReward to set
	 *
	 */
	public void setSmallReward(boolean smallReward) {
		this.smallReward = smallReward;
	}

	/**
	 * get smallRewardExpiryStartDateTime
	 * @return the smallRewardExpiryStartDateTime
	 *
	 */
	public Date getSmallRewardExpiryStartDateTime() {
		return smallRewardExpiryStartDateTime;
	}

	/**
	 * set smallRewardExpiryStartDateTime
	 * @param smallRewardExpiryStartDateTime the smallRewardExpiryStartDateTime to set
	 *
	 */
	public void setSmallRewardExpiryStartDateTime(
			Date smallRewardExpiryStartDateTime) {
		this.smallRewardExpiryStartDateTime = smallRewardExpiryStartDateTime;
	}

	/**
	 * get smallRewardExpiryEndDateTime
	 * @return the smallRewardExpiryEndDateTime
	 *
	 */
	public Date getSmallRewardExpiryEndDateTime() {
		return smallRewardExpiryEndDateTime;
	}

	/**
	 * set smallRewardExpiryEndDateTime
	 * @param smallRewardExpiryEndDateTime the smallRewardExpiryEndDateTime to set
	 *
	 */
	public void setSmallRewardExpiryEndDateTime(Date smallRewardExpiryEndDateTime) {
		this.smallRewardExpiryEndDateTime = smallRewardExpiryEndDateTime;
	}

	/**
	 * get smallRewardDescription
	 * @return the smallRewardDescription
	 *
	 */
	public String getSmallRewardDescription() {
		return smallRewardDescription;
	}

	/**
	 * set smallRewardDescription
	 * @param smallRewardDescription the smallRewardDescription to set
	 *
	 */
	public void setSmallRewardDescription(String smallRewardDescription) {
		this.smallRewardDescription = smallRewardDescription;
	}

	/**
	 * get monkPay
	 * @return the monkPay
	 *
	 */
	public Double getMonkPay() {
		return monkPay;
	}

	/**
	 * set monkPay
	 * @param monkPay the monkPay to set
	 *
	 */
	public void setMonkPay(Double monkPay) {
		this.monkPay = monkPay;
	}

	/**
	 * get platformFee
	 * @return the platformFee
	 *
	 */
	public Double getPlatformFee() {
		return platformFee;
	}

	/**
	 * set platformFee
	 * @param platformFee the platformFee to set
	 *
	 */
	public void setPlatformFee(Double platformFee) {
		this.platformFee = platformFee;
	}

	/**
	 * get totalCharges
	 * @return the totalCharges
	 *
	 */
	public Double getTotalCharges() {
		return totalCharges;
	}

	/**
	 * set totalCharges
	 * @param totalCharges the totalCharges to set
	 *
	 */
	public void setTotalCharges(Double totalCharges) {
		this.totalCharges = totalCharges;
	}

	/**
	 * get guaranteeRewards
	 * @return the guaranteeRewards
	 *
	 */
	public boolean isGuaranteeRewards() {
		return guaranteeRewards;
	}

	/**
	 * set guaranteeRewards
	 * @param guaranteeRewards the guaranteeRewards to set
	 *
	 */
	public void setGuaranteeRewards(boolean guaranteeRewards) {
		this.guaranteeRewards = guaranteeRewards;
	}

	/**
	 * get totalRewardAmount
	 * @return the totalRewardAmount
	 *
	 */
	public Double getTotalRewardAmount() {
		if( competitive1st != null ) {
			totalRewardAmount += competitive1st;
		}
		if( competitive2nd != null ) {
			totalRewardAmount += competitive2nd;
		}
		if( competitive3rd != null ) {
			totalRewardAmount += competitive3rd;
		}
		if( awardByViews != null ) {
			totalRewardAmount += awardByViews;
		}
		if( awardByLikes != null ) {
			totalRewardAmount += awardByLikes;
		}
		if( fixedPrizeAmount != null && fixedPrizeQuantity != null ) {
			totalRewardAmount += fixedPrizeAmount*fixedPrizeQuantity;				
		}
		return totalRewardAmount;
	}

	/**
	 * set totalRewardAmount
	 * @param totalRewardAmount the totalRewardAmount to set
	 *
	 */
	public void setTotalRewardAmount(Double totalRewardAmount) {
		this.totalRewardAmount = totalRewardAmount;
	}

	/**
	 * get numberOfViews
	 * @return the numberOfViews
	 *
	 */
	public int getNumberOfViews() {
		return numberOfViews;
	}

	/**
	 * set numberOfViews
	 * @param numberOfViews the numberOfViews to set
	 *
	 */
	public void setNumberOfViews(int numberOfViews) {
		this.numberOfViews = numberOfViews;
	}

	/**
	 * get x1
	 * @return the x1
	 *
	 */
	public String getX1() {
		return x1;
	}

	/**
	 * set x1
	 * @param x1 the x1 to set
	 *
	 */
	public void setX1(String x1) {
		this.x1 = x1;
	}

	/**
	 * get x2
	 * @return the x2
	 *
	 */
	public String getX2() {
		return x2;
	}

	/**
	 * set x2
	 * @param x2 the x2 to set
	 *
	 */
	public void setX2(String x2) {
		this.x2 = x2;
	}

	/**
	 * get y1
	 * @return the y1
	 *
	 */
	public String getY1() {
		return y1;
	}

	/**
	 * set y1
	 * @param y1 the y1 to set
	 *
	 */
	public void setY1(String y1) {
		this.y1 = y1;
	}

	/**
	 * get y2
	 * @return the y2
	 *
	 */
	public String getY2() {
		return y2;
	}

	/**
	 * set y2
	 * @param y2 the y2 to set
	 *
	 */
	public void setY2(String y2) {
		this.y2 = y2;
	}

	/**
	 * get w
	 * @return the w
	 *
	 */
	public String getW() {
		return w;
	}

	/**
	 * set w
	 * @param w the w to set
	 *
	 */
	public void setW(String w) {
		this.w = w;
	}

	/**
	 * get h
	 * @return the h
	 *
	 */
	public String getH() {
		return h;
	}

	/**
	 * set h
	 * @param h the h to set
	 *
	 */
	public void setH(String h) {
		this.h = h;
	}

	/**
	 * get competitive1stPaid
	 * @return the competitive1stPaid
	 *
	 */
	public boolean isCompetitive1stPaid() {
		return competitive1stPaid;
	}

	/**
	 * set competitive1stPaid
	 * @param competitive1stPaid the competitive1stPaid to set
	 *
	 */
	public void setCompetitive1stPaid(boolean competitive1stPaid) {
		this.competitive1stPaid = competitive1stPaid;
	}

	/**
	 * get competitive2ndPaid
	 * @return the competitive2ndPaid
	 *
	 */
	public boolean isCompetitive2ndPaid() {
		return competitive2ndPaid;
	}

	/**
	 * set competitive2ndPaid
	 * @param competitive2ndPaid the competitive2ndPaid to set
	 *
	 */
	public void setCompetitive2ndPaid(boolean competitive2ndPaid) {
		this.competitive2ndPaid = competitive2ndPaid;
	}

	/**
	 * get competitive3rdPaid
	 * @return the competitive3rdPaid
	 *
	 */
	public boolean isCompetitive3rdPaid() {
		return competitive3rdPaid;
	}

	/**
	 * set competitive3rdPaid
	 * @param competitive3rdPaid the competitive3rdPaid to set
	 *
	 */
	public void setCompetitive3rdPaid(boolean competitive3rdPaid) {
		this.competitive3rdPaid = competitive3rdPaid;
	}

	/**
	 * get awardByViewsPaid
	 * @return the awardByViewsPaid
	 *
	 */
	public boolean isAwardByViewsPaid() {
		return awardByViewsPaid;
	}

	/**
	 * set awardByViewsPaid
	 * @param awardByViewsPaid the awardByViewsPaid to set
	 *
	 */
	public void setAwardByViewsPaid(boolean awardByViewsPaid) {
		this.awardByViewsPaid = awardByViewsPaid;
	}

	/**
	 * get awardByLikesPaid
	 * @return the awardByLikesPaid
	 *
	 */
	public boolean isAwardByLikesPaid() {
		return awardByLikesPaid;
	}

	/**
	 * set awardByLikesPaid
	 * @param awardByLikesPaid the awardByLikesPaid to set
	 *
	 */
	public void setAwardByLikesPaid(boolean awardByLikesPaid) {
		this.awardByLikesPaid = awardByLikesPaid;
	}

	/**
	 * get fixedPrizeAmountPaid
	 * @return the fixedPrizeAmountPaid
	 *
	 */
	public boolean isFixedPrizeAmountPaid() {
		return fixedPrizeAmountPaid;
	}

	/**
	 * set fixedPrizeAmountPaid
	 * @param fixedPrizeAmountPaid the fixedPrizeAmountPaid to set
	 *
	 */
	public void setFixedPrizeAmountPaid(boolean fixedPrizeAmountPaid) {
		this.fixedPrizeAmountPaid = fixedPrizeAmountPaid;
	}

	/**
	 * get fixedPrizeQuantityPaid
	 * @return the fixedPrizeQuantityPaid
	 *
	 */
	public Integer getFixedPrizeQuantityPaid() {
		return fixedPrizeQuantityPaid;
	}

	/**
	 * set fixedPrizeQuantityPaid
	 * @param fixedPrizeQuantityPaid the fixedPrizeQuantityPaid to set
	 *
	 */
	public void setFixedPrizeQuantityPaid(Integer fixedPrizeQuantityPaid) {
		this.fixedPrizeQuantityPaid = fixedPrizeQuantityPaid;
	}

	/**
	 * get paid
	 * @return the paid
	 *
	 */
	public boolean isPaid() {
		return Paid;
	}

	/**
	 * set paid
	 * @param paid the paid to set
	 *
	 */
	public void setPaid(boolean paid) {
		Paid = paid;
	}	

}