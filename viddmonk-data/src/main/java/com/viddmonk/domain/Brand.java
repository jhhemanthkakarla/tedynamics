package com.viddmonk.domain;
/**
 * Copyright Viddmonk 2013
 * 
 */

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Brand domain class which represents Brand Table in MongoDB
 * @author rvadlam
 *
 */
@Document(collection = "brands")
@JsonIgnoreProperties({"campaigns" })
public class Brand implements Serializable {

	private static final long serialVersionUID = 8172458786791528664L;

	@Id
	private String id;
	
	@NotNull
	@NotEmpty
	@Size(min=1, max=128)	
	private String name;
	
	@Size(max=256)	
    private String shortDescription;	
	
	@Size(max=2000)	
    private String description;	
	
	@Size(max=128)	
	@Email
	private String email;
	
	@Size(max=128)	
	private String website;
	
	@Size(max=128)	
	private String facebook;
	
	@Size(max=128)	
	private String twitter;

	@DBRef
	private Brand parentBrand;
	
	@DBRef
	private Set<Media> imageList = new HashSet<Media>();

	/**
	 * get id
	 * @return the id
	 *
	 */
	public String getId() {
		return id;
	}

	/**
	 * set id
	 * @param id the id to set
	 *
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * get name
	 * @return the name
	 *
	 */
	public String getName() {
		return name;
	}

	/**
	 * set name
	 * @param name the name to set
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * get description
	 * @return the description
	 *
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * set description
	 * @param description the description to set
	 *
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * get email
	 * @return the email
	 *
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * set email
	 * @param email the email to set
	 *
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * get website
	 * @return the website
	 *
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * set website
	 * @param website the website to set
	 *
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * get parentBrand
	 * @return the parentBrand
	 *
	 */
	public Brand getParentBrand() {
		return parentBrand;
	}

	/**
	 * set parentBrand
	 * @param parentBrand the parentBrand to set
	 *
	 */
	public void setParentBrand(Brand parentBrand) {
		this.parentBrand = parentBrand;
	}

	/**
	 * get imageList
	 * @return the imageList
	 *
	 */
	public Set<Media> getImageList() {
		return imageList;
	}

	/**
	 * set imageList
	 * @param imageList the imageList to set
	 *
	 */
	public void setImageList(Set<Media> imageList) {
		this.imageList = imageList;
	}

	/**
	 * get shortDescription
	 * @return the shortDescription
	 *
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * set shortDescription
	 * @param shortDescription the shortDescription to set
	 *
	 */
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	/**
	 * get facebook
	 * @return the facebook
	 *
	 */
	public String getFacebook() {
		return facebook;
	}

	/**
	 * set facebook
	 * @param facebook the facebook to set
	 *
	 */
	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	/**
	 * get twitter
	 * @return the twitter
	 *
	 */
	public String getTwitter() {
		return twitter;
	}

	/**
	 * set twitter
	 * @param twitter the twitter to set
	 *
	 */
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Brand [id=" + id + ", name=" + name + ", description="
				+ description + ", email=" + email + ", website=" + website
				+ "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((website == null) ? 0 : website.hashCode());
		return result;
	}
}