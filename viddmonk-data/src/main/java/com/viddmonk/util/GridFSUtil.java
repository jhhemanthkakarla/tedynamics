package com.viddmonk.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
 
/**
 * GridFs Writer
 * 
 */
public class GridFSUtil {
 
    public static void main(String[] args) {
    	GridFSUtil gridFsWriter = new GridFSUtil();
    	//gridFsWriter.writeImageFile();
    	//gridFsWriter.readfile();
    	//gridFsWriter.writeAudioFile();
    	//gridFsWriter.readAudiofile();
    	//gridFsWriter.writeVideoFile();
    	gridFsWriter.readVideofile();

    }

    private void writeImageFile() {

    	@SuppressWarnings("resource")
		ApplicationContext ctx = 
	                     new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
	                      (GridFsOperations) ctx.getBean("gridFsTemplate");
	 
		DBObject metaData = new BasicDBObject();
		metaData.put("sports", "image 1");
		metaData.put("tennis", "federer");
	 
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream("/Users/rvadlam/Downloads/image2.png");
			gridOperations.store(inputStream, "image2.png", "image/png", metaData);
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Done");    	
    }

    private void writeAudioFile() {

    	@SuppressWarnings("resource")
		ApplicationContext ctx = 
	                     new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
	                      (GridFsOperations) ctx.getBean("gridFsTemplate");
	 
		DBObject metaData = new BasicDBObject();
		metaData.put("sports", "audio 1");
		metaData.put("tennis", "federer");
	 
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream("/Users/rvadlam/Downloads/Play_Ball.mp3");
			gridOperations.store(inputStream, "Play_Ball.mp3", metaData);
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Done");    	
    }

    private void writeVideoFile() {

    	@SuppressWarnings("resource")
		ApplicationContext ctx = 
	                     new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
	                      (GridFsOperations) ctx.getBean("gridFsTemplate");
	 
		DBObject metaData = new BasicDBObject();
		metaData.put("sports", "video 2");
		metaData.put("rafting", "rafting");
	 
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream("/Users/rvadlam/Downloads/101_0096.mp4");
			gridOperations.store(inputStream, "101_0096.mp4", metaData);
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Done");    	
    }
    
    private void readfile() {
		ApplicationContext ctx = 
                new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
                 (GridFsOperations) ctx.getBean("gridFsTemplate");
   
    	List<GridFSDBFile> result = gridOperations.find(
                 new Query().addCriteria(Criteria.where("filename").is("image2.png")));
   
	  	for (GridFSDBFile file : result) {
	  		try {
	  			System.out.println(file.getFilename());
	  			System.out.println(file.getContentType());
	   
	  			//save as another image
	  			file.writeTo("/Users/rvadlam/Downloads/image2_mongoread.png");
	  		} catch (IOException e) {
	  			e.printStackTrace();
	  		}
	  	}
   
	  	System.out.println("Done");
    }
    
    private void readAudiofile() {
		ApplicationContext ctx = 
                new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
                 (GridFsOperations) ctx.getBean("gridFsTemplate");
   
    	List<GridFSDBFile> result = gridOperations.find(
                 new Query().addCriteria(Criteria.where("filename").is("Play_ball.mp3")));
   
	  	for (GridFSDBFile file : result) {
	  		try {
	  			System.out.println("file name: " + file.getFilename());
	  			System.out.println("file content type: " + file.getContentType());
	  			System.out.println("file content length: " + file.getLength());
	  			
	  			//save as another image
	  			file.writeTo("/Users/rvadlam/Downloads/Play_Ball_myfile.mp3");
	  		} catch (IOException e) {
	  			e.printStackTrace();
	  		}
	  	}
   
	  	System.out.println("Done");
    }
    
    private void readVideofile() {
		ApplicationContext ctx = 
                new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
                 (GridFsOperations) ctx.getBean("gridFsTemplate");
   
    	List<GridFSDBFile> result = gridOperations.find(
                 new Query().addCriteria(Criteria.where("filename").is("101_0096.mp4")));
   
	  	for (GridFSDBFile file : result) {
	  		try {
	  			System.out.println("file name: " + file.getFilename());
	  			System.out.println("file content type: " + file.getContentType());
	  			System.out.println("file content length: " + file.getLength());
	  			
	  			//save as another image
	  			file.writeTo("/Users/rvadlam/Downloads/101_0096_mongoread.mp4");
	  		} catch (IOException e) {
	  			e.printStackTrace();
	  		}
	  	}
   
	  	System.out.println("Done");
    }

}
