package com.viddmonk.dto;


/**
 * File Params
 * @author rvadlam
 *
 */
public class FileParams {
	
	private String fileName;
	private String fileType;
	private String fileLocation;
	
	/**
	 * get fileName
	 * @return the fileName
	 *
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * set fileName
	 * @param fileName the fileName to set
	 *
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * get fileType
	 * @return the fileType
	 *
	 */
	public String getFileType() {
		return fileType;
	}
	
	/**
	 * set fileType
	 * @param fileType the fileType to set
	 *
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	/**
	 * get fileLocation
	 * @return the fileLocation
	 *
	 */
	public String getFileLocation() {
		return fileLocation;
	}
	
	/**
	 * set fileLocation
	 * @param fileLocation the fileLocation to set
	 *
	 */
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
}