/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;

/**
 * Campaign Repository
 * @author rvadlam
 *
 */
@Repository
public interface CampaignRepository extends MongoRepository<Campaign, String>, QueryDslPredicateExecutor<Campaign>  {
	
	Campaign findByTitle(String title);
	
	Page<Campaign> findByBrand(Brand brand, Pageable pageable);

}
