/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.Media;

/**
 * Brand Repository
 * @author rvadlam
 *
 */
@Repository
public interface MediaRepository extends MongoRepository<Media, String>, QueryDslPredicateExecutor<Media>  {
}
