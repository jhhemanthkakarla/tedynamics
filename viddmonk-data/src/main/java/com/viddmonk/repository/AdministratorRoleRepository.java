/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.AdministratorRole;

/**
 * Administrator Repository
 * @author rvadlam
 *
 */
@Repository
public interface AdministratorRoleRepository extends MongoRepository<AdministratorRole, String>, QueryDslPredicateExecutor<AdministratorRole>  {

	AdministratorRole findByRole(String role);
	
}
