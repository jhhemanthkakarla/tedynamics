/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.viddmonk.domain.PaymentCard;

/**
 * Payment Card Repository
 * @author rvadlam
 *
 */
@Repository
public interface PaymentCardRepository extends MongoRepository<PaymentCard, String>, QueryDslPredicateExecutor<PaymentCard>  {
	
	PaymentCard findByNickname(String nickname);
	
	PaymentCard findById(String id);
	
	List<PaymentCard> findAll();

}
