/* use this when you want to set a poster using JWPLayer */
function setposter(playerid,filename)
{
    jwplayer(playerid).setup({
        file: filename,
        width: "100%",
        aspectratio: "1:1",
        fallback: 'true',
        autostart: 'true',
        type:"mp4",
        controls: 'false',
        mute: 'true',
        icons:'false',
        events: {
            onDisplayClick: function(event) {
                jwplayer(playerid).play();
            }
        }
    });
    setTimeout(function() {
        jwplayer(playerid).pause();
        jwplayer().setMute(false);
        jwplayer().setControls(false);
    },2000);

};

/* use this when you have a poster. Event is working in FF but not in Chrome*/
function playvideo(playerid,filename,posterurl)
{
    jwplayer(playerid).setup({
        file: filename,
        width: "100%",
        aspectratio: "1:1",
        autostart: 'true',
        type:'mp4',
        icons:'false',
        events: {
            onComplete: function() {
                /* jwplayer(playerid).remove(); */
                replaceposter(playerid,posterurl);
            }
            /* enable this if you want to pause on the last frame
            onTime: function(object) {
                if(object.position > object.duration - 1) {this.pause();
                }
            }
            */
        }
    });
}

function replaceposter(playerid,posterurl) {
    console.log("replace the poster");
    /* need to replace this div with the poster */
}
