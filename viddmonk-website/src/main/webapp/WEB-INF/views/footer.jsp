
<div class="extra">
	<div class="container">
		<div class="row">
			<div class="span3">
				<h4>About</h4>
				<ul>
					<li><a href="javascript:;">About Us</a></li>
					<li><a href="javascript:;">Twitter</a></li>
					<li><a href="javascript:;">Facebook</a></li>
					<li><a href="javascript:;">Google+</a></li>
				</ul>
			</div> <!-- /span3 -->
			<div class="span3">
				<h4>Support</h4>
				<ul>
					<li><a href="javascript:;">Frequently Asked Questions</a></li>
					<li><a href="javascript:;">Ask a Question</a></li>
					<li><a href="javascript:;">Video Tutorial</a></li>
					<li><a href="javascript:;">Feedback</a></li>
				</ul>
			</div> <!-- /span3 -->
			<div class="span3">
				<h4>Legal</h4>
				<ul>
					<li><a href="javascript:;">License</a></li>
					<li><a href="javascript:;">Terms of Use</a></li>
					<li><a href="javascript:;">Privacy Policy</a></li>
					<li><a href="javascript:;">Security</a></li>
				</ul>
			</div> <!-- /span3 -->
			<div class="span3">
				<h4>Settings</h4>
				<ul>
					<li><a href="javascript:;">Consectetur adipisicing</a></li>
					<li><a href="javascript:;">Eiusmod tempor </a></li>
					<li><a href="javascript:;">Fugiat nulla pariatur</a></li>
					<li><a href="javascript:;">Officia deserunt</a></li>
				</ul>				
			</div> <!-- /span3 -->
		</div> <!-- /row -->
	</div> <!-- /container -->
</div> <!-- /extra -->
    
<div class="footer">		
	<div class="container">
		<div class="row">
			<div id="footer-copyright" class="span6">
				&copy; 2013-14 Viddmonk.
			</div> <!-- /span6 -->			
		</div> <!-- /row -->
	</div> <!-- /container -->
</div> <!-- /footer -->

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<c:url value="/resources/js/libs/jquery-1.8.3.min.js"/>"></script>
<script src="<c:url value="/resources/js/libs/jquery-ui-1.10.0.custom.min.js"/>"></script>
<script src="<c:url value="/resources/js/libs/bootstrap.min.js"/>"></script>
<script src="<c:url value="/resources/js/plugins/flot/jquery.flot.js"/>"></script>
<script src="<c:url value="/resources/js/plugins/flot/jquery.flot.pie.js"/>"></script>
<script src="<c:url value="/resources/js/plugins/flot/jquery.flot.resize.js"/>"></script>
<script src="<c:url value="/resources/js/Application.js"/>"></script>
<script src="<c:url value="/resources/js/charts/area.js"/>"></script>
<script src="<c:url value="/resources/js/charts/donut.js"/>"></script>
<script src="<c:url value="/resources/js/ajax-form.js"/>"></script>
