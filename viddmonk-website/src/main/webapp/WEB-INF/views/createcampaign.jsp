<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>viddMonk create campaign - name and company</title>
	    <%@include file="head-createcampaign.jsp" %>
	</head>

<body>

    <%@include file="topnav.jsp" %>

    <!-- SECTION-2: MAIN CONTAINER AND SIDEBAR NAV -->
    <div class="main-container" id="main-container">
                    
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-blue widget-header-flat">
                                            <h4 class="lighter">New Campaign</h4>
                                        </div>
                                        <div class="widget-body ">
                                            <div class="widget-main">
                                                <div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
                                                    <ul class="wizard-steps">
                                                        <li data-target="#step1" class="active">
                                                            <span class="step">1</span>
                                                            <span class="title">Setup</span>
                                                        </li>

                                                        <li data-target="#step2">
                                                            <span class="step">2</span>
                                                            <span class="title">Describe</span>
                                                        </li>

                                                        <li data-target="#step3">
                                                            <span class="step">3</span>
                                                            <span class="title">Reward</span>
                                                        </li>

                                                        <li data-target="#step4">
                                                            <span class="step">4</span>
                                                            <span class="title">Pay</span>
                                                        </li>
                                                    </ul>
                                           			<div class="header smaller lighter center green"></div>
                                                </div>

                                                <div class="step-content row-fluid position-relative" id="step-container">

                                                    <div class="step-pane active" id="step1">
                                                        <form name="createcampaignform" id="createcampaignform" class="form-horizontal" method="POST" action="<c:url value="/secure/campaigns/add"/>" accept-charset="UTF-8"  enctype="multipart/form-data">
	                                                        <input type="hidden" id="campaignId" name="campaignId" value="${campaign.id}" /> 
	                                                        <input type="hidden" id="step" name="step" value="1" /> 
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label" for="form-field-select-11">Create a new company profile</label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-9 no-padding-left">
                                                                        <select class="form-control" id="brandId" name="brandId">
	                                                                        <option value=""> - Create New or Select -</option>
                                                                        	<c:forEach items="${myBrandList}" var="brand">
	                                                                        	<c:if test="${brand.id == campaign.brand.id}">
		                                                                            <option selected="selected" value="${brand.id}">${brand.name}</option>
		                                                                        </c:if>
	                                                                        	<c:if test="${brand.id != campaign.brand.id}">
		                                                                            <option value="${brand.id}">${brand.name}</option>
		                                                                        </c:if>
		                                                                        <c:if test="${brand.id == newbrandId}">
		                                                                            <option selected="selected" value="${brand.id}">${brand.name}</option>
		                                                                        </c:if>
	 																		</c:forEach>                                                                                                   
                                                                        </select>
                                                                    </div>
		                                                           <div class="btn-group center">
		                                                                <button class="btn btn-vdsalmon" data-toggle="modal" data-target="#modal-form-brand" id="create-brand">
		                                                                    <span class="icon-pencil white"></span>
		                                                                </button>
		                                                                <button class="btn btn-vdsalmon" data-toggle="modal" data-target="#modal-form-brand" id="edit-brand">
		                                                                    <span class="icon-plus white"></span>
		                                                                </button>		
		                                                            </div>                                                                    
                                                                </div>
                                                            </div>
	                                                            <div class="form-group">
	                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Campaign Title </label>
	                                                                <div class="col-sm-8">
	                                                                    <input type="text" id="title" name="title" value="${campaign.title}" placeholder="Short name for campaign" class="form-control limited" maxlength="100"/> 
	                                                                </div>
	                                                            </div>
		
	                                                            <div class="form-group">
	                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-3"> Brief Description </label>
	                                                                <div class="col-sm-8">
	                                                                    <textarea class="form-control limited" id="description" name="description" placeholder="What kind of videos are you looking for ?" maxlength="256"> ${campaign.description}</textarea>
	                                                                </div>
	                                                            </div>	
	                                                            <div class="form-group">
	                                                                <label class="col-sm-3 control-label no-padding-right" for="id-date-range-picker-1">Start and End Dates</label>
	
	                                                                <div class="row">
	                                                                    <div class="col-xs-8 col-sm-4">
	                                                                        <div class="input-group">
	                                                                                            <span class="input-group-addon">
	                                                                                                <i class="icon-calendar bigger-110"></i>
	                                                                                            </span>
	                                                                            <input class="form-control" type="text" name="date-range-picker" id="id-date-range-picker-1" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${campaign.startDateTime}" /> <fmt:formatDate pattern="MM/dd/yyyy" value="${campaign.endDateTime}" /> "/>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>	                                                           
						                                        <div class="row-fluid wizard-actions">
						                                            <button class="btn btn-vddarkgrey" onclick="return save();">
						                                                <i class="icon-save"></i>
						                                                Save
						                                            </button>
						                                            <button class="btn btn-prev btn-vdlightgrey" disabled="disabled">
						                                                <i class="icon-arrow-left"></i>
						                                                Prev
						                                            </button>
						                                            <button class="btn btn-next btn-vdsalmon" data-last="Finish " onclick="return next();">
						                                                Next
						                                                <i class="icon-arrow-right icon-on-right"></i>
						                                            </button>
						                                        </div>
				                                         </form>
                                                    </div>
                                                    

                                                    <!-- MODAL DIALOG FOR BRAND -->
                                                    <div id="modal-form-brand" class="modal" tabindex="-1">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="green bigger">Brand</h4>
                                                                </div>
                                                                <div class="modal-body overflow-visible">
																    <form class="form-horizontal well" name="brandform" id="brandform" data-async data-target="#modal-form-brand" action="<c:url value="/secure/brands/add/campaign/submit?campaignId=${campaign.id}" />" method="POST" accept-charset="UTF-8"  enctype="multipart/form-data">
                                                                         <input type="hidden" id="id" name="id" value="" />
                                                                         <div class="form-group">
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-name"> Name of Brand </label>
                                                                                <div class="col-sm-8 ">
                                                                                    <input type="text" id="form-field-brand-name" name="name" placeholder="Name of brand" class="form-control limited" maxlength="100"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-desc"> Brief Description </label>
                                                                                <div class="col-sm-8">
                                                                                    <textarea class="form-control limited" id="form-field-brand-desc" name="shortDescription" placeholder="Brief Description" maxlength="256"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-logo"> Logo </label>
                                                                                <div class="col-sm-8">
                                                                                    <input type="file" id="form-field-brand-logo" name="imageFile">
                                                                                </div>
                                                                            </div>

											 							    <c:if test="${brand.s3URL != null}">
	                                                                            <div class="form-group">
	                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-logo">Preview </label>
	                                                                                <div class="col-sm-8 ">
	                                                                                    <img src="${brand.s3URL}" />
	                                                                                </div>
	                                                                            </div>
																		    </c:if>                        
                                                                            
                                                                            <h3 class="header smaller-90 green">
                                                                                Optional Information
                                                                            </h3>

                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-desc"> URL </label>
                                                                                <div class="col-sm-8">
                                                                                    <span class="input-icon">
                                                                                        <input type="text" id="form-field-brand-url" name="website">
                                                                                        <i class="icon-globe"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-desc"> Facebook </label>
                                                                                <div class="col-sm-8">
                                                                                    <span class="input-icon">
                                                                                        <input type="text" id="form-field-brand-fb" name="facebook">
                                                                                        <i class="icon-facebook"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-brand-desc" name="twitter"> Twitter </label>
                                                                                <div class="col-sm-8">
                                                                                    <span class="input-icon">
                                                                                        <input type="text" id="form-field-brand-tw" name="twitter">
                                                                                        <i class="icon-twitter"></i>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
							                                                <div class="row-fluid wizard-actions">
							                                                    <button class="btn btn-info" type="reset">
							                                                        <i class="icon-reset"></i>
							                                                        Reset
							                                                    </button>
									                                            <button class="btn btn-vddarkgrey">
							                                                        Save
							                                                        <i class="icon-save icon-on-right"></i>
							                                                    </button>
							                                                 </div>                                                                            
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                            </div><!-- widget main -->
                                        </div><!-- widget body -->
                                    </div><!-- widget box -->
                                </div>
                            </div>
                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->                
    </div><!-- /.main-container-inner -->

    <%@include file="createcampaignfooter.jsp" %>
        
</body>
</html>