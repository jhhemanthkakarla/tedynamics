<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>viddMonk create campaign - Campaign Assets</title>
	    <%@include file="head-createcampaign.jsp" %>
	</head>

<body>

    <%@include file="topnav.jsp" %>


    <!-- SECTION-2: MAIN CONTAINER AND SIDEBAR NAV -->
    <div class="main-container" id="main-container">
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-blue widget-header-flat">
                                            <h4 class="lighter">New Campaign</h4>
                                        </div>
                                        <div class="widget-body ">
                                            <div class="widget-main">
                                                <div id="fuelux-wizard" class="row-fluid" data-target="#step-container">
                                                    <ul class="wizard-steps">
                                                        <li data-target="#step1" class="complete">
                                                            <a href="<c:url value="/secure/campaigns/update/${campaign.id}"/>"><span class="step">1</span></a>
                                                            <span class="title">Setup</span>
                                                        </li>

                                                        <li data-target="#step2" class="complete">
                                                        	<a href="<c:url value="/secure/campaigns/update/${campaign.id}?step=2"/>"><span class="step">2</span></a>
                                                            <span class="title">Describe</span>
                                                        </li>

                                                        <li data-target="#step3" class="active">
                                                            <span class="step">3</span>
                                                            <span class="title">Reward</span>
                                                        </li>

                                                        <li data-target="#step4">
                                                            <span class="step">4</span>
                                                            <span class="title">Pay</span>
                                                        </li>
                                                    </ul>
													<div class="header smaller lighter center green"></div>
                                                </div>
	                                               <div class="step-pane" id="step3"> <!-- STEP-3 SELECT CAMPAIGN TYPE -->
	                                                   <form name="createcampaignform" id="createcampaignform" class="form-horizontal" method="POST" action="<c:url value="/secure/campaigns/add"/>" accept-charset="UTF-8"  enctype="multipart/form-data">
	                                                       <input type="hidden" id="campaignId" name="campaignId" value="${campaign.id}" /> 
	                                                       <input type="hidden" id="brandId" name="brandId" value="${campaign.brand.id}" /> 
	                                                       <input type="hidden" id="step" name="step" value="4" /> 
				                                           <div class="row">
				                                               <div class="form-group">
				                                                   <div class="col-sm-4 control-group">
				                                                       <h5 class="header smaller center blue no-margin-top">
					                                                       <div class="checkbox no-padding-left no-padding-top">
		                                                                        <label>
		                                                                        	<c:if test="${campaign.competitive == true }">
		                                         	                                   <input id="form-field-reward-type-comp" name="competitive" type="checkbox" checked="checked" class="ace" />
		                                                                        	</c:if>
		                                                                        	<c:if test="${campaign.competitive == false }">
		                                         	                                   <input id="form-field-reward-type-comp" name="competitive" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                            <span class="lbl"> Competitive (1st, 2nd, 3rd best etc.) </span>
		                                                                        </label>
		                                                                   </div>
				                                                       </h5>
				                                                       <h5 class="bolder no-margin-top"> Amount Rewarded: </h5>
				                                                       <div class="space-6"></div>
				                                                       <div id="options-competitive">
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-comp-1st"> 1st </label>
		                                                                        <div class="col-sm-8">
		                                                                            <span class="input-icon">
		                                                                                <input type="text" value="${campaign.competitive1st}" id="form-field-comp-1st" name="competitive1st" placeholder="$Amount">
		                                                                                <i class="icon-dollar"></i>
		                                                                            </span>
		                                                                        </div>
		                                                                    </div>
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-comp-2nd"> 2nd </label>
		                                                                        <div class="col-sm-8">
		                                                                                <span class="input-icon">
		                                                                                    <input type="text" value="${campaign.competitive2nd}" id="form-field-comp-2nd" name="competitive2nd" placeholder="$Amount">
			                                                                                <i class="icon-dollar"></i>
		                                                                                </span>
		                                                                        </div>
		                                                                    </div>
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-comp-3rd"> 3rd </label>
		                                                                        <div class="col-sm-8">
		                                                                                <span class="input-icon">
		                                                                                    <input type="text" value="${campaign.competitive3rd}" id="form-field-comp-3rd" name="competitive3rd" placeholder="$Amount">
			                                                                                <i class="icon-dollar"></i>
		                                                                                </span>
		                                                                        </div>
		                                                                    </div>
		                                                                </div>
				                                                     </div>
				                                                     <div class="col-sm-4 control-group">
		                                                                <h5 class="header smaller blue center no-margin-top">
		                                                                    <div class="checkbox no-padding-left no-padding-top">
		                                                                        <label>
		                                                                        	<c:if test="${campaign.competitive == true }">
		                                                                            <input id="form-field-reward-type-views" checked="checked" name="performance" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                        	<c:if test="${campaign.competitive == false }">
		                                                                            <input id="form-field-reward-type-views" name="performance" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                            <span class="lbl"> Performance based (Views, Likes) </span>
		                                                                        </label>
		                                                                    </div>
		                                                                </h5>
		                                                                <h5 class="bolder no-margin-top"> Amount Rewarded: </h5>
		                                                                <div class="space-6"></div>
				                                                        <div id="options-performance">
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-comp-views"> Views </label>
		                                                                        <div class="col-sm-8">
		                                                                            <span class="input-icon">
		                                                                                <input type="text" value="${campaign.awardByViews}" id="form-field-comp-views" name="awardByViews" placeholder="$Amount">
		                                                                                <i class="icon-dollar"></i>
		                                                                            </span>
		                                                                        </div>
		                                                                    </div>
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-comp-likes"> Likes </label>
		                                                                        <div class="col-sm-8">
		                                                                            <span class="input-icon">
		                                                                                <input type="text" id="form-field-comp-likes" value="${campaign.awardByLikes}" name="awardByLikes" placeholder="$Amount">
		                                                                                <i class="icon-dollar"></i>
		                                                                            </span>
		                                                                        </div>
		                                                                    </div>
				                                                         </div>
				                                                      </div>
				                                                      <div class="col-sm-4 control-group">
		                                                                <h5 class="header smaller blue center no-margin-top">
		                                                                    <div class="checkbox no-padding-left no-padding-top">
		                                                                        <label>
		                                                                        	<c:if test="${campaign.competitive}">
			                                                                            <input id="form-field-reward-type-fixed" checked="checked" name="fixedPrize" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                        	<c:if test="${!campaign.competitive}">
			                                                                            <input id="form-field-reward-type-fixed" name="fixedPrize" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                            <span class="lbl"> Fixed prize/per video selected </span>
		                                                                        </label>
		                                                                    </div>
		                                                                </h5>
				                                                        <h5 class="bolder no-margin-top"> Amount Rewarded: </h5>
				                                                        <div class="space-6"></div>
				                                                        <div id="options-fixed">
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-comp-views"> Prize </label>
		                                                                        <div class="col-sm-8">
		                                                                            <span class="input-icon">
		                                                                                <input type="text" id="form-field-fixed-prize" value="${campaign.fixedPrizeAmount}" name="fixedPrizeAmount" placeholder="$/video">
		                                                                                <i class="icon-dollar"></i>
		                                                                            </span>
		                                                                        </div>
		                                                                    </div>
		                                                                    <div class="form-group">
		                                                                        <label class="col-sm-2 control-label pull-left" for="form-field-fixed-prize-quantity"> Quantity </label>
		                                                                        <div class="col-sm-8">
		                                                                            <span class="input-icon">
		                                                                                <input type="text" id="form-field-fixed-prize-quantity" value="${campaign.fixedPrizeQuantity}" name="fixedPrizeQuantity" placeholder="# of videos">
		                                                                            </span>
		                                                                        </div>
		                                                                    </div>
				                                                         </div>
				                                                      </div>
				                                                   </div>
				                                                   <h5 class="header smaller blue center no-margin-top">
			                                                            <div class="checkbox no-padding-left no-padding-top">
			                                                                <label>
			                                                                        <c:if test="${campaign.competitive}">
					                                                                    <input name="form-field-reward-type-views" checked="checked" name="smallReward" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                        	<c:if test="${!campaign.competitive}">
					                                                                    <input name="form-field-reward-type-views" name="smallReward" type="checkbox" class="ace" />
		                                                                        	</c:if>		                                                           
			                                                                    <span class="lbl"> A small reward for approved videos perhaps ?</span>
			                                                                </label>
			                                                            </div>
				                                                   </h5>
				                                                   <div class="form-group" id="approved-video-reward">
			                                                            <div class="form-group">
			                                                                <label class="col-sm-3 control-label no-padding-right"> Brief Description </label>
			                                                                <div class="col-sm-8">
			                                                                    <textarea class="form-control limited" id="form-field-approved-video-reward" name="smallRewardDescription" placeholder="Brief Description for all approved videos (e.g. 5% off next purchase" maxlength="256"></textarea>
			                                                                </div>
			                                                            </div>
			                                                            <div class="form-group">
			                                                                <label class="col-sm-3 control-label no-padding-right" for="id-date-range-picker-1">Start and End Dates</label>			
			                                                                <div class="row">
			                                                                    <div class="col-xs-8 col-sm-4">
			                                                                        <div class="input-group">
			                                                                                            <span class="input-group-addon">
			                                                                                                <i class="icon-calendar bigger-110"></i>
			                                                                                            </span>
			                                                                            <input class="form-control" type="text" name="smallreward-date-range-picker" id="id-date-range-picker-approved-video-reward" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${campaign.smallRewardExpiryStartDateTime}" />-<fmt:formatDate pattern="MM/dd/yyyy" value="${campaign.smallRewardExpiryEndDateTime}" /> "/>
			                                                                        </div>
			                                                                    </div>
			                                                                </div>
			                                                            </div>	                                                           


				                                                     </div>
				                                                     <h5 class="header vdTitle smaller no-margin-top"></h5>
				                                                     <div class="form-group">
			                                                            <label class="col-sm-3 control-label no-padding-right">Total Campaign Cost (FYI)</label>
			                                                            <div class="col-sm-8 control-group">
			                                                                <div class="form-group">
			                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-total-prizeamount"> Total Prize Amount </label>
			                                                                    <div class="col-sm-8">
			                                                                        <input readonly type="text" id="form-field-total-prizeamount" value="$Monk pay">
			                                                                    </div>
			                                                                </div>
			                                                                <div class="form-group">
			                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-total-service"> Cost of service </label>
			                                                                    <div class="col-sm-8">
			                                                                        <input readonly type="text" id="form-field-total-service" value="$viddmonk platform">
			                                                                    </div>
			                                                                </div>
			                                                                <div class="form-group">
			                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-total-cost"> Total Campaign Cost </label>
			                                                                    <div class="col-sm-8">
			                                                                        <input disabled type="text" id="form-field-total-cost" value="$cost of campaign">
			                                                                    </div>
			                                                                </div>
			                                                            </div>
				                                                     </div>
				                                                     <h5 class="header green smaller no-margin-top"></h5>
			                                                        <div class="form-group">
			                                                            <label class="col-sm-3 control-label no-padding-right">Guarantee Rewards ?</label>
			                                                            <div class="col-sm-8 control-group">
			                                                                <label>
			                                                                        <c:if test="${campaign.competitive}">
					                                                                    <input id="form-field-reward-guarantee" checked="checked" name="guaranteeRewards" value="${campaign.guaranteeRewards}" type="checkbox" class="ace" />
		                                                                        	</c:if>
		                                                                        	<c:if test="${!campaign.competitive}">
					                                                                    <input id="form-field-reward-guarantee" name="guaranteeRewards" value="${campaign.guaranteeRewards}" type="checkbox" class="ace" />
		                                                                        	</c:if>		                                                           
			                                                                    <span class="lbl"> Yes, I guarantee </span>
			                                                                </label>
			                                                            </div>
			                                                        </div>					
							                                        <div class="row-fluid wizard-actions">
							                                            <button class="btn btn-vddarkgrey" onclick="return save3();">
							                                                <i class="icon-save"></i>
							                                                Save
							                                            </button>
							                                            <button class="btn btn-prev btn-vdlightgrey"  onclick="return save2();">
							                                                <i class="icon-arrow-left"></i>
							                                                Prev
							                                            </button>
							                                            <button class="btn btn-next btn-vdsalmon growl-type" data-type="success" data-last="Finish " onclick="return finish();">
							                                                Finish
							                                                <i class="icon-arrow-right icon-on-right"></i>
							                                            </button>
							                                        </div>
				                                                 </div>
				                                             </form>
				                                         </div> <!-- end of step-3 -->
                                            </div><!-- widget main -->
                                        </div><!-- widget body -->
                                    </div><!-- widget box -->
                                </div>
                            </div>
                            <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->                
    </div><!-- /.main-container-inner -->

    <%@include file="createcampaignfooter.jsp" %>
        
</body>
</html>