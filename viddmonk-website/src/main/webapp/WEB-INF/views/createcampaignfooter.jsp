<!-- ======== BASIC SCRIPTS ==========-->
    <!-- basic scripts -->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>'>"+"<"+"/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-1.10.2.min.js"/>'>"+"<"+"/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='<c:url value="/resources/assets/js/jquery.mobile.custom.min.js"/>'>"+"<"+"/script>");
    </script>
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/typeahead-bs2.min.js"/>"></script>

    <!-- page specific plugin scripts -->
    <script src="<c:url value="/resources/assets/js/jquery-ui-1.10.3.custom.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.ui.touch-punch.min.js"/>"></script>

    <script src="<c:url value="/resources/assets/js/chosen.jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/date-time/bootstrap-datepicker.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/date-time/bootstrap-timepicker.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/date-time/moment.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/date-time/daterangepicker.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/bootstrap-colorpicker.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/bootstrap-tag.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.knob.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.autosize.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.inputlimiter.1.3.1.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.maskedinput.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.validate.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/fuelux/fuelux.wizard.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/fuelux/fuelux.spinner.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/additional-methods.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/bootbox.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.maskedinput.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/select2.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/jquery.colorbox-min.js"/>"></script>
	<script src="<c:url value="/resources/assets/js/jquery.Jcrop.min.js"/>"></script>


    <!-- ace scripts -->

    <script src="<c:url value="/resources/assets/js/ace-elements.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/ace.min.js"/>"></script>

<!-- inline scripts related to this page -->

<script type="text/javascript">
jQuery(function($) {
    // Cropping calls
    var jcrop_api;

    $('#poster-original').Jcrop({
        onChange:   showCoords,
        onSelect:   showCoords,
        onRelease:  clearCoords
    },function(){
        jcrop_api = this;
    });

    $('#poster-cropped').on('change','input',function(e){
        var x1 = $('#x1').val(),
                x2 = $('#x2').val(),
                y1 = $('#y1').val(),
                y2 = $('#y2').val();
        jcrop_api.setSelect([x1,y1,x2,y2]);
    });



    $('.trigger').click(function() {
        $('.content').hide();
        $('.' + $(this).data('rel')).show();
    });

    $(".select2").css('width','200px').select2({allowClear:true})
            .on('change', function(){
                $(this).closest('form').validate().element($(this));
            });

    $('#fuelux-wizard').ace_wizard().on('change' , function(e, info){

    }).on('finished', function(e) {
                bootbox.dialog({
                    message: "Thank you! Your information was successfully saved!",
                    buttons: {
                        "success" : {
                            "label" : "OK",
                            "className" : "btn-sm btn-primary"
                        }
                    }
                });
            }).on('stepclick', function(e){
            });

    $('#modal-wizard .modal-header').ace_wizard();
    $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');


    $('#id-input-file-1 , #id-input-file-2').ace_file_input({
        no_file:'No File ...',
        btn_choose:'Choose',
        btn_change:'Change',
        droppable:false,
        onchange:null,
        thumbnail:false 
    });

    $('#form-field-brand-logo').ace_file_input({
        no_file:'Logo file ...',
        btn_choose:'Choose',
        btn_change:'Change',
        droppable:false,
        onchange:null,
        thumbnail:false, //| true | large,
        whitelist:'gif|png|jpg|jpeg',
        blacklist:'exe|php'
        //onchange:''
        //
    });

    $('#id-input-file-3').ace_file_input({
        style:'well',
        btn_choose:'Drop files here or click to choose',
        btn_change:null,
        no_icon:'icon-cloud-upload',
        droppable:true,
        thumbnail:'small',
        preview_error : function(filename, error_code) {
        }

    }).on('change', function(){
            });
    //dynamically change allowed formats by changing before_change callback function
    $('#id-file-format').removeAttr('checked').on('change', function() {
        var before_change
        var btn_choose
        var no_icon
        if(this.checked) {
            btn_choose = "Drop images here or click to choose";
            no_icon = "icon-picture";
            before_change = function(files, dropped) {
                var allowed_files = [];
                for(var i = 0 ; i < files.length; i++) {
                    var file = files[i];
                    if(typeof file === "string") {
                        //IE8 and browsers that don't support File Object
                        if(! (/\.(jpe?g|png|gif|bmp)$/i).test(file) ) return false;
                    }
                    else {
                        var type = $.trim(file.type);
                        if( ( type.length > 0 && ! (/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
                                || ( type.length == 0 && ! (/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
                                ) continue;//not an image so don't keep this file
                    }

                    allowed_files.push(file);
                }
                if(allowed_files.length == 0) return false;

                return allowed_files;
            }
        }
        else {
            btn_choose = "Drop files here or click to choose";
            no_icon = "icon-cloud-upload";
            before_change = function(files, dropped) {
                return files;
            }
        }
        var file_input = $('#id-input-file-3');
        file_input.ace_file_input('update_settings', {'before_change':before_change, 'btn_choose': btn_choose, 'no_icon':no_icon})
        file_input.ace_file_input('reset_input');

    });
    $("#topnavinclude").load("topnavbar.html");


    $("#modal-form-brand").modal({
        keyboard: true,
        backdrop: 'static',
        show: false,
        open: initBrandDialog()
    });

    $("#form-field-select-11").change(function () {
       initBrandDialog();
    });

    $( "#create-brand" )
    .button()
    .click(function() {
        var selectedBrandId = $('#brandId').val();
    	<c:forEach items="${myBrandList}" var="brand">
    		if( "${brand.id}" == selectedBrandId ) {
    			$("#id").val("${brand.id}");
    			$("#form-field-brand-name").val("${brand.name}");
    			$("#form-field-brand-desc").val("${brand.shortDescription}");
    			$("#form-field-brand-url").val("${brand.website}");
    			$("#form-field-brand-fb").val("${brand.facebook}");
    			$("#form-field-brand-tw").val("${brand.twitter}");
        	}
		</c:forEach>                                                                                                   

    });
    
    $( "#edit-brand" )
    .button()
    .click(function() {
        var selectedBrandId = $('#brandId').val();
    	<c:forEach items="${myBrandList}" var="brand">
    		if( "${brand.id}" == selectedBrandId ) {
    			$("#id").val("${brand.id}");
    			$("#form-field-brand-name").val("");
    			$("#form-field-brand-desc").val("");
    			$("#form-field-brand-url").val("");
    			$("#form-field-brand-fb").val("");
    			$("#form-field-brand-tw").val("");
        	}
		</c:forEach>                                                                                                   

    });
})

$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
    $(this).prev().focus();
});
$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
    $(this).next().focus();
});

$('#spinner1').ace_spinner({value:0,min:0,max:60,step:1, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
        .on('change', function(){
            // alert(this.value)
        });

// for the tags
$(".chosen-select").chosen();
$('#chosen-multiple-style').on('click', function(e){
    var target = $(e.target).find('input[type=radio]');
    var which = parseInt(target.val());
    if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
    else $('#form-field-select-4').removeClass('tag-input-style');
});


//we could just set the data-provide="tag" of the element inside HTML, but IE8 fails!
var tag_input = $('#form-field-tags');
if(! ( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())) )
{
    tag_input.tag(
            {
                placeholder:tag_input.attr('placeholder'),
                //enable typeahead by specifying the source array
                source: ace.variable_US_STATES,//defined in ace.js >> ace.enable_search_ahead
            }
    );
}
else {
    //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
    tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
    //$('#form-field-tags').autosize({append: "\n"});
}

//this is for the gallery implementation
var colorbox_params = {
    reposition:true,
    scalePhotos:true,
    scrolling:false,
    previous:'<i class="icon-arrow-left"></i>',
    next:'<i class="icon-arrow-right"></i>',
    close:'&times;',
    current:'{current} of {total}',
    maxWidth:'100%',
    maxHeight:'100%',
    onOpen:function(){
        document.body.style.overflow = 'hidden';
    },
    onClosed:function(){
        document.body.style.overflow = 'auto';
    },
    onComplete:function(){
        $.colorbox.resize();
    }
};

$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon



//switch element when editing inline
function aceSwitch( cellvalue, options, cell ) {
    setTimeout(function(){
        $(cell) .find('input[type=checkbox]')
                .wrap('<label class="inline" />')
                .addClass('ace ace-switch ace-switch-5')
                .after('<span class="lbl"></span>');
    }, 0);
}
//enable datepicker
function pickDate( cellvalue, options, cell ) {
    setTimeout(function(){
        $(cell) .find('input[type=text]')
                .datepicker({format:'yyyy-mm-dd' , autoclose:true});
    }, 0);
}


// Simple event handler, called from onChange and onSelect
// event handlers, as per the Jcrop invocation above
function showCoords(c)
{
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
};

function clearCoords()
{
    $('#coords input').val('');
};
//end of JCrop functions

//navButtons

function style_edit_form(form) {
    //enable datepicker on "sdate" field and switches for "stock" field
    form.find('input[name=sdate]').datepicker({format:'yyyy-mm-dd' , autoclose:true})
            .end().find('input[name=stock]')
            .addClass('ace ace-switch ace-switch-5').wrap('<label class="inline" />').after('<span class="lbl"></span>');

    //update buttons classes
    var buttons = form.next().find('.EditButton .fm-button');
    buttons.addClass('btn btn-sm').find('[class*="-icon"]').remove();//ui-icon, s-icon
    buttons.eq(0).addClass('btn-primary').prepend('<i class="icon-ok"></i>');
    buttons.eq(1).prepend('<i class="icon-remove"></i>')

    buttons = form.next().find('.navButton a');
    buttons.find('.ui-icon').remove();
    buttons.eq(0).append('<i class="icon-chevron-left"></i>');
    buttons.eq(1).append('<i class="icon-chevron-right"></i>');
}

function style_delete_form(form) {
    var buttons = form.next().find('.EditButton .fm-button');
    buttons.addClass('btn btn-sm').find('[class*="-icon"]').remove();//ui-icon, s-icon
    buttons.eq(0).addClass('btn-danger').prepend('<i class="icon-trash"></i>');
    buttons.eq(1).prepend('<i class="icon-remove"></i>')
}

function style_search_filters(form) {
    form.find('.delete-rule').val('X');
    form.find('.add-rule').addClass('btn btn-xs btn-primary');
    form.find('.add-group').addClass('btn btn-xs btn-success');
    form.find('.delete-group').addClass('btn btn-xs btn-danger');
}
function style_search_form(form) {
    var dialog = form.closest('.ui-jqdialog');
    var buttons = dialog.find('.EditTable')
    buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'icon-retweet');
    buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'icon-comment-alt');
    buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'icon-search');
}

function beforeDeleteCallback(e) {
    var form = $(e[0]);
    if(form.data('styled')) return false;

    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
    style_delete_form(form);

    form.data('styled', true);
}

function beforeEditCallback(e) {
    var form = $(e[0]);
    form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
    style_edit_form(form);
}



//it causes some flicker when reloading or navigating grid
//it may be possible to have some custom formatter to do this as the grid is being created to prevent this
//or go back to default browser checkbox styles for the grid
function styleCheckbox(table) {
}


//unlike navButtons icons, action icons in rows seem to be hard-coded
//you can change them like this in here if you want
function updateActionIcons(table) {
}

//replace icons with FontAwesome icons like above
function updatePagerIcons(table) {
    var replacement =
    {
        'ui-icon-seek-first' : 'icon-double-angle-left bigger-140',
        'ui-icon-seek-prev' : 'icon-angle-left bigger-140',
        'ui-icon-seek-next' : 'icon-angle-right bigger-140',
        'ui-icon-seek-end' : 'icon-double-angle-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
        var icon = $(this);
        var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

        if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
    })
}

function enableTooltips(table) {
    $('.navtable .ui-pg-button').tooltip({container:'body'});
    $(table).find('.ui-pg-div').tooltip({container:'body'});
}

function initBrandDialog() {
    console.log("Initializing brand modal elements");
}
//var selr = jQuery(grid_selector).jqGrid('getGridParam','selrow');

</script>
	    
    <script type="text/javascript">
	    <!--
	    function save()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "1";
	        document.createcampaignform.submit();
	        return true;
	    }
	    function save2()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "2";
	        document.createcampaignform.submit();
	        return true;
	    }
	    function save3()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "3";
	        document.createcampaignform.submit();
	        return true;
	    }
	    function save4()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "4";
	        document.createcampaignform.submit();
	        return true;
	    }
	
	    function next()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "2";
	        document.createcampaignform.submit();
	        return true;
	    }
		
	    function next2()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "3";
	        document.createcampaignform.submit();
	        return true;
	    }
		
	    function next3()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "4";
	        document.createcampaignform.submit();
	        return true;
	    }
		
	    function finish()
	    {
	        document.createcampaignform.action = "<c:url value="/secure/campaigns/save"/>";
	        document.createcampaignform.step.value = "finish";
	        document.createcampaignform.submit();
	        return true;
	    }
	    -->

	</script>
	
	
	<script type="text/javascript">	
		$(document).ready(function() {
			// check name availability on focus lost
			$('#name').blur(function() {
				if ($('#name').val()) {	
					checkAvailability();
				}
			});
			$("#account").submit(function() {
				var account = $(this).serializeObject();
				$.postJSON("account", account, function(data) {
					$("#assignedId").val(data.id);
					showPopup();
				});
				return false;				
			});
		});
	</script>