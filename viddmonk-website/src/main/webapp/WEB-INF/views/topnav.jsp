<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <!-- SECTION-1: TOPBAR NAV -->
    <div class="navbar navbar-default vdTitleBox" id="navbar">
	    <script type="text/javascript">
            try{ace.settings.check('navbar' , 'fixed')}catch(e){}
        </script>

        <div class="navbar-container no-padding-right" id="navbar-container">
            <div class="navbar-header pull-left">
                <a href="<c:url value="/dashboard"/>" class="navbar-brand">
                    <small class="bolder">                        
                        <img src="<c:url value="/resources/assets/images/vdMonkg-logo30.png"/>" alt="viddMonk">
                        viddmonk
                    </small>
                </a><!-- /.brand -->
            </div><!-- /.navbar-header -->

            <div class="navbar-header pull-right" role="navigation">
                <ul class="nav ace-nav">
		            <li class="salmon">
                		 <a href="<c:url value="/dashboard"/>">
                    		<i class="icon-search"></i>
                		</a>
            		</li>
		            <li class="salmon">
		                <a href="<c:url value="/dashboard"/>">
		                    <i class="icon-dashboard"></i>
		                </a>
		            </li>
		            <li class="salmon">
		                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
		                    <i class="icon-bullhorn"></i>
		                    <span class="badge badge-important">4</span>
		                </a>
		                <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
		                    <li>
		                        <a href="<c:url value="/dashboard"/>">
		                            <i class="icon-dashboard"></i>
		                            View All
		                        </a>
		                    </li>
		
		                    <li>
		                        <a href="secure/campaigns/add">
		                            <i class="icon-plus"></i>
		                            Create New
		                        </a>
		                    </li>
		                </ul>
		            </li>
		            <li class="salmon">
		                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
		                    <i class="icon-bell-alt icon-animated-bell"></i>
		                    <span class="badge badge-important">0</span>
		                </a>
		
		                <ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
		                    <li class="dropdown-header">
		                        <i class="icon-warning-sign"></i>
							    <c:if test="${empty totalNotifications}">
		                     	    0 Notifications
		   						</c:if>                        
							    <c:if test="${not empty totalNotifications}">
		                     	    ${totalNotifications} Notifications
							    </c:if>                        
		                    </li>
		
						    <c:if test="${empty mynotifications}">
									No New Notifications
						    </c:if>
							<c:forEach items="${mynotifications}" var="notification">
		                           <li>
		                               <a href="#">
		                                   <div class="clearfix">
		                                       <span class="pull-left">
		                                           <i class="btn btn-xs no-hover btn-pink icon-comment"></i>
		                                           ${notification.message}
		                                       </span>
		                                   </div>
		                               </a>
		                           </li>
							</c:forEach>                            
		                </ul>
		            </li>
		            <li class="salmon">
		                <a data-toggle="dropdown" href="#" class="dropdown-toggle">
		
		                    <i class="icon-user"></i>
		                    <i class="icon-caret-down"></i>
		                </a>
		
		                <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
		                    <li>
		                        <a href="#">
		                            <i class="icon-cog"></i>
		                            Settings
		                        </a>
		                    </li>
		
		                    <li>
		                        <a href="#">
		                            <i class="icon-user"></i>
		                            Profile
		                        </a>
		                    </li>
		
		                    <li class="divider"></li>
		
		                    <li>
		                        <a href="<c:url value="/resources/j_spring_security_logout" />">
		                            <i class="icon-off"></i>
		                            Logout
		                        </a>
		                    </li>
		                </ul><!-- /.ace-nav -->
		             </li>
		         </ul>
            </div><!-- /.navbar-header -->
        </div><!-- /.container -->
    </div>
    <!-- end of top-nav bar -->
