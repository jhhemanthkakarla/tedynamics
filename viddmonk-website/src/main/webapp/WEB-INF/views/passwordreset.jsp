<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Viddmonk</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
    <link href='<c:url value="/resources/css/bootstrap.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/reset.css"/>' >
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/theme.css"/>">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
</head>

<body>    

 	<div id="box_reset">
        <div class="container">
            <div class="span12 box_wrapper">
                <div class="span12 box">
                    <div>
                        <div class="head">
                            <h4>Enter your email address below to receive instructions on how to reset your password.</h4>
                        </div>
                        <div class="form">
                            <form method="POST" action="<c:url value="/login/passwordreset/complete"/>" accept-charset="UTF-8" class="separate-sections fill-up validatable">
                                <input type="text" name="email" placeholder="Email address"/>
                                <input type="submit" class="btn" value="Reset password"/>
                            </form>
                        </div>
                    </div>
                </div>
                <p class="already">Know your password? <a href="<c:url value="/auth/login"/>"> Sign in</a></p>
            </div>
        </div>
    </div>
    
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src='<c:url value="/resources/js/bootstrap.min.js"/>' ></script>
    <script src='<c:url value="/resources/js/theme.js"/>'></script>
</body>
</html>