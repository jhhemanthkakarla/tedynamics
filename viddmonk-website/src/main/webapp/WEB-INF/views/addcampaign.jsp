<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Viddmonk - Add a Campaign</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />    
    
    <link href='<c:url value="/resources/css/bootstrap.min.css"/>' rel="stylesheet">
    <link href='<c:url value="/resources/css/bootstrap-responsive.min.css"/>' rel="stylesheet">    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href='<c:url value="/resources/css/font-awesome.min.css"/>' rel="stylesheet">        
    <link href='<c:url value="/resources/css/ui-lightness/jquery-ui-1.10.0.custom.min.css"/>' rel="stylesheet">            
    <link href='<c:url value="/resources/css/base-admin-2.css"/>' rel="stylesheet">            
    <link href='<c:url value="/resources/css/base-admin-2-responsive.css"/>' rel="stylesheet">                
    <link href='<c:url value="/resources/css/pages/dashboard.css"/>' rel="stylesheet">                
    <link href='<c:url value="/resources/css/custom.css"/>' rel="stylesheet">                
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>
    <%@include file="topnav.jsp" %>
    
    <div class="main">
	    <div class="container">
			<div class="row">
				<div class="span12">
		      		<div class="widget stacked">			
						<div class="widget-header">
							<i class="icon-check"></i>
							<h3>Add A Campaign</h3>
						</div> <!-- /widget-header -->
						<div class="widget-content">
							<br />
                            <form method="POST" action="<c:url value="/secure/campaigns/add/submit"/>" accept-charset="UTF-8" class="separate-sections fill-up validatable" enctype="multipart/form-data">
								<fieldset>
									<div class="control-group">
										<label class="control-label" for="validateSelect">Select Brand if Applicable</label>
										<div class="controls">									
											<select id="brandId" name="brandId">
													<option value="" />
													<c:forEach items="${myBrandList}" var="brand">
														<option value="${brand.id}" />${brand.name}
													</c:forEach>
											</select>
										</div>								
				         			</div>
									<div class="control-group">
									  <label class="control-label" for="name">Title</label>
									  <div class="controls">
									    <input type="text" class="input-large" name="title" id="title" />
									  </div>
									</div>
									<div class="control-group">
									  <label class="control-label" for="tagline">Tag line</label>
									  <div class="controls">
									    <input type="text" class="input-large" name="tagline" id="tagline" />
									  </div>
									</div>
									<div class="control-group">
									  <label class="control-label" for="shortDescription">Short Description</label>
									  <div class="controls">
									    <input type="text" class="input-large" name="shortDescription" id="shortDescription" />
									  </div>
									</div>
								    <div class="control-group">
								      <label class="control-label" for="longDescription">Long Description</label>
								      <div class="controls">
								        <textarea class="span4" name="longDescription" id="longDescription" rows="4"></textarea>
								      </div>
								    </div>
								    <div class="control-group">
								      <label class="control-label" for="instructions">Instructions</label>
								      <div class="controls">
								        <textarea class="span4" name="instructions" id="instructions" rows="4"></textarea>
								      </div>
								    </div>
								    <div class="control-group">
								      <label class="control-label" for="longDescription">Upload Images</label>
								      <div class="controls">
											<input multiple="multiple" type="file" id="imageFiles" name="imageFiles" />
								      </div>
								    </div>
								    <div class="control-group">
								      <label class="control-label" for="longDescription">Upload Audio Files</label>
								      <div class="controls">
											<input multiple="multiple" type="file" id="audioFiles" name="audioFiles" />
								      </div>
								    </div>
								    <div class="control-group">
								      <label class="control-label" for="longDescription">Upload Video Files</label>
								      <div class="controls">
											<input multiple="multiple" type="file" id="videoFiles" name="videoFiles" />
								      </div>
								    </div>
								    <div class="form-actions">
								      <button type="submit" class="btn btn-success btn">Save</button>&nbsp;&nbsp;
								      <button type="reset" class="btn">Cancel</button>
								    </div>
						  		</fieldset>
							</form>
						</div> <!-- /widget-content -->				
					</div> <!-- /widget -->					
	    		</div> <!-- /span12 -->     	
      		</div> <!-- /row -->
    	</div> <!-- /container -->
	</div> <!-- /main -->  
    
    <%@include file="footer.jsp" %>
</body>
</html>