<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>
	<meta charset="utf-8" />
	<title>viddmonk - Login</title>
	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<!-- basic styles -->
	<link href="<c:url value="/resources/assets/images/favicon.ico"/>" rel="icon" type="image/x-icon" />
	<link href="<c:url value="/resources/assets/css/bootstrap.min.css"/>" rel="stylesheet" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/viddmonk.min.css"/>" />
	
	<!--[if IE 7]>
	  <link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome-ie7.min.css"/>" />
	<![endif]-->
	<!-- page specific plugin styles -->
	<!-- fonts -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-fonts.css"/>" />
	<!-- ace styles -->
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/ace.min.css"/>" />
	<link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-rtl.min.css"/>" />
	<!--[if lte IE 8]>
	  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
	<![endif]-->
	<!-- inline styles related to this page -->
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

	<body class="login-layout">
		<div class="main-container viddlogin">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
                                    <img src="<c:url value="/resources/assets/images/vdMonkg-logo45.png"/>" alt="viddMonk">
									<small class="bolder white">viddmonk</small>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border vdSalmon">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header vdTitle lighter bigger center">
												<i class="icon-gears"></i>
												 Enter Information to Login
											</h4>

											<div class="space-6"></div>

				                            <form method="POST" action="<c:url value="/resources/j_spring_security_check"/>" accept-charset="UTF-8" class="separate-sections fill-up validatable">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="j_username" class="form-control" placeholder="Username" />
  															<i class="icon-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="j_password" class="form-control" placeholder="Password" />
															<i class="icon-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" />
															<span class="lbl"> Remember Me</span>
														</label>

														<button type="submit" class="width-35 pull-right btn btn-sm btn-success">
															<i class="icon-key"></i>
															Login
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>

											<div class="social-or-login center">
												<span class="bigger-110 vdTitle">Or Login Using</span>
											</div>
											<div class="space-6"></div>
											<div class="center">
												<a class="btn btn-primary width-20">
													<i class="icon-facebook"></i>
												</a>

												<a class="btn btn-info width-20">
													<i class="icon-twitter"></i>
												</a>

												<a class="btn btn-danger width-20">
													<i class="icon-google-plus"></i>
												</a>
											</div>
										</div><!-- /widget-main -->

										<div class="toolbar vdSalmon clearfix">
											<div>
												<a href="<c:url value="/login/passwordreset" />" onclick="show_box('forgot-box'); return false;" class="forgot-password-link">
													<i class="icon-arrow-left"></i>
													I forgot my password
												</a>
											</div>
											<div>
												<a href="<c:url value="/signup"/>" onclick="show_box('signup-box'); return false;" class="user-signup-link">
													I want to register
													<i class="icon-arrow-right"></i>
												</a>
											</div>
										</div>
									</div><!-- /widget-body -->
								</div><!-- /login-box -->
								

								<div id="forgot-box" class="forgot-box widget-box no-border vdSalmon">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="icon-key"></i>
												Retrieve Password
											</h4>

											<div class="space-6"></div>
											<p>
												Enter your email and to receive instructions
											</p>

                            				<form method="POST" action="<c:url value="/login/passwordreset/complete"/>" accept-charset="UTF-8" class="separate-sections fill-up validatable">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" name="email" id="email" class="form-control" placeholder="Email" />
															<i class="icon-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="submit" class="width-35 pull-right btn btn-sm btn-danger">
															<i class="icon-lightbulb"></i>
															Send Me!
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /widget-main -->

										<div class="toolbar center">
											<a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
												Back to login
												<i class="icon-arrow-right"></i>
											</a>
										</div>
									</div><!-- /widget-body -->
								</div><!-- /forgot-box -->

								<div id="signup-box" class="signup-box widget-box no-border vdSalmon">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header lighter bigger center vdTitle">
												<i class="icon-user"></i>
												New User Registration
											</h4>

											<div class="space-6"></div>
											<p> Enter your details to begin: </p>
				                            <form method="POST" action="<c:url value="/register"/>" accept-charset="UTF-8" class="separate-sections fill-up validatable">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" name="email" class="form-control" placeholder="Email" />
															<i class="icon-envelope"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="name" class="form-control" placeholder="Name (optional)" />
															<i class="icon-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
							                               <input type="text" name="zip" class="form-control" placeholder="Zipcode (optional)"/>
															<i class="icon-map-marker"></i>
														</span>
													</label>

													<label class="block">
														<input type="checkbox" class="ace" />
														<span class="lbl">
															I accept the
															<a href="#">User Agreement</a>
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="reset" class="width-30 pull-left btn btn-sm">
															<i class="icon-refresh"></i>
															Reset
														</button>

														<button type="submit" class="width-65 pull-right btn btn-sm btn-success">
															Register
															<i class="icon-arrow-right icon-on-right"></i>
														</button>
													</div>
												</fieldset>
											</form>
										</div>

										<div class="toolbar center vdSalmon">
											<a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
												<i class="icon-arrow-left"></i>
												Back to login
											</a>
										</div>
									</div><!-- /widget-body -->
								</div><!-- /signup-box -->
								
								
							</div><!-- /position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-1.10.2.min.js"/>'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<c:url value="/resources/assets/js/jquery.mobile.custom.min.js"/>'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			function show_box(id) {
			 jQuery('.widget-box.visible').removeClass('visible');
			 jQuery('#'+id).addClass('visible');
			}
		</script>
	</body>
</html>