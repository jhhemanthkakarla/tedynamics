<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>viddMonk - campaign public page</title>
	    <%@include file="head-campaigndashboard.jsp" %>
	    <script type="text/javascript" src="<c:url value="/resources/js/instafeed.min.js"/>"></script>
	    <script type="text/javascript" charset="utf-8">
			FB.init({
			    appId: '593979387351326', 
			    status: true, 
			    cookie: true,
			    xfbml: true
			});

			function share_prompt()
			{
			    FB.ui(
			       {
			         method: 'feed',
			         name: '${campaign.title}',
			         link: 'http://viddmonk.com:8080<c:url value="/public/campaigns/public/${campaign.id}" />',
			         picture: 
							<c:forEach items="${campaign.campaignImageList}" var="media">                        
			         			'${media.s3URL}'
                 			</c:forEach>      	         
				         ,
			         caption: '${campaign.title}',
			         description: '${campaign.description}',
			         message: '${campaign.tags}'
			       },
			       function(response) {
			         if (response && response.post_id) {
			           alert('Post was published.');
			         } else {
			           alert('Post was not published.');
			         }
			       }
			     );
			 }
		</script>
	</head>

<body>
	<%@include file="facebook_init.jsp" %>
	<script>
	    FB.login(function(response) {
	    	   if (response.authResponse) {
		    	 alert("Welcome!  Fetching your information.... ")  
	    	     console.log('Welcome!  Fetching your information.... ');
	    	     FB.api('/me', function(response) {
	    	       console.log('Good to see you, ' + response.name + '.');
	    	     });
	    	   } else {
			    	 alert("Welcome!  Fetching your information2.... ")  
	    	     console.log('User cancelled login or did not fully authorize.');
	    	   }
	    	 });
	</script>
	<script type="text/javascript">
	    var feed = new Instafeed({
	        get: 'tagged',
	        tagName: '${campaign.tags}',
	        clientId: '72b33f66c112462983a484fa3791335e'
	    });
	    feed.run();
	</script>
	
    <jsp:include page="campaignpublictopnav.jsp">
    	<jsp:param name="tabitem" value="dashboard" />
    </jsp:include>
    <div class="main-container" id="main-container">
        <div class="page-content"><!-- PAGE CONTENT BEGINS -->
            <header>
                <h1 class="toupper align-center white">${campaign.title}</h1>
                <p class="lead align-center white">${campaign.description}</p>
                <div class="text-center">
                    <div class="btn-group">
                        <button class="btn">
							<a href="#" onclick="share_prompt()"><span style="color: white;"><i class="icon-facebook"></i></span></a>
                        </button>
                        <button class="btn">
							<a href="https://www.facebook.com/sharer.php?s=100&p[url]=http://viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>=100&t=View Campaign&p[title]=View Campaign&p[summary]=${fn:replace($campaign.tags,'#', '')}" 
								onclick="window.open('https://www.facebook.com/sharer.php?s=100&t=View Campaign&p[title]=View Campaign&p[summary]=${fn:replace(campaign.tags,'#', '')}&p[url]=http://viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>','popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';" ><span style="color: white;"><i class="icon-facebook"></i></span></a>
                        </button>
                        <button class="btn" >                            
							<a href="https://twitter.com/share?url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;text=View Campaign &amp;hashtags=${fn:replace($campaign.tags,'#', '')}'" 
								onclick="window.open('https://twitter.com/share?url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&text=View Campaign&hashtags=${fn:replace(campaign.tags,'#', '')}','popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';"><span style="color: white;"><i class="icon-twitter"></i></span></a>
                        </button>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        
                        <button class="btn">
							<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;title=View Campaign&amp;summary=${fn:replace(campaign.tags,'#', '')}&amp;source=http://www.viddmonk.com" 
							onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;title=View Campaign&amp;summary=${fn:replace(campaign.tags,'#', '')}&amp;source=http://www.viddmonk.com'),'popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';" target="_blank">
							<span style="color: white;"><i class="icon-linkedin"></i></span></a>                            
                        </button>
                        <button class="btn">
							<a href="https://plus.google.com/share?data-contentdeeplinkid=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;content=View Campaign &amp;hashtags=${fn:replace($campaign.tags,'#', '')}'" 
								onclick="window.open('https://plus.google.com/share?url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&content=View Campaign&hashtags=${fn:replace(campaign.tags,'#', '')}','popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';"><span style="color: white;"><i class="icon-google-plus"></i></span></a>
                        </button>
                        <button class="btn">
                            <i class="icon-link"></i>
                        </button>
                    </div>
                </div>
                <p class="white text-center">${campaign.tags}</p>

            </header>
            <div class="page-content-inner">
                <div class="row">
                    <div class="col-md-6 col-lg-5">
						<c:forEach items="${campaign.campaignImageList}" var="media">                        
							<img src="${media.s3URL}">
							<% request.setAttribute("campaignURL", "${media.s3URL}"); %>
	                    </c:forEach>      
                    </div>
                    <div class="col-md-6 col-lg-7">
                        <div class="widget-box light-border">
                            <div class="widget-header header-color-dark">
                                <h5 class="smaller"> Details</h5>
                                <div class="widget-toolbar">
                                    <span class="badge badge-pink">
                                     <c:if test="${empty campaign.campaignStatus || campaign.campaignStatus == 'CREATED'}">
                                     	<span class="label label-sm label-pink">Created </span>
                                     </c:if>
                                     <c:if test="${campaign.campaignStatus == 'LIVE'}">
                                     	<span class="label label-sm label-pink">Live </span>
                                     </c:if>
                                     <c:if test="${campaign.campaignStatus == 'CLOSED'}">
                                     	<span class="label label-sm label-pink">Closed </span>
                                     </c:if>
                                    </span>
                                </div>
                            </div>
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class="title-with-lines title-left " style="padding: 6px 0px">Time Left</div>
                                    <p class="lead"> 6 days, 5 hrs, 30 mins.</p>

                                    <div class="title-with-lines title-left " style="padding: 6px 0px">
                                        Rewards, Instructions
                                        <i class="icon-check vdTitle"></i>   <!-- WHEN THE CAMPAIGN IS GUARANTEED -->
                                    </div>
                                    <div class="widget-box transparent collapsed">
                                        <div class="widget-header">
		                                    <c:if test="${campaign.totalRewardAmount != null}">
	                                            <h4 class="vdTitle">
	                                                Total: &#36;${campaign.totalRewardAmount}
	                                            </h4>
		                                    </c:if>
		                                    <c:if test="${campaign.totalRewardAmount == null}">
	                                            <h4 class="vdTitle">
	                                                Total: &#36;0
	                                            </h4>
		                                    </c:if>
                                            <div class="widget-toolbar ">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="row">
                                                   	<c:if test="${campaign.competitive == true }">
	                                                    <div class="col-sm-4">
	                                                        <h3>Competitive</h3>
	                                                        <ul class="lead">
	                                                            <li>First Prize: &#36;${campaign.competitive1st}</li>
	                                                            <li>Second Prize: &#36;${campaign.competitive2nd}</li>
	                                                            <li>Third Prize: &#36;${campaign.competitive3rd}</li>
	                                                        </ul>
	                                                    </div>
                                                   	</c:if>
                                                   	<c:if test="${campaign.performance == true }">
	                                                    <div class="col-sm-4">
	                                                        <h3>Performance</h3>
	                                                        <ul class="lead">
	                                                            <li>Views: &#36;${campaign.awardByViews}</li>
	                                                            <li>Likes: &#36;${campaign.awardByLikes}</li>
	                                                        </ul>
	                                                    </div>
                                                   	</c:if>
                                                   	<c:if test="${campaign.fixedPrize == true }">
	                                                    <div class="col-sm-4">
	                                                        <h3>Fixed</h3>
	                                                        <ul class="lead">
	                                                            <li>Prize/video: &#36;${campaign.fixedPrizeAmount}</li>
	                                                            <li>Quantity: ${campaign.fixedPrizeQuantity}</li>
	                                                        </ul>
	                                                    </div>
                                                   	</c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="align-left center">${campaign.description}</span>
                                    <div class="widget-box transparent collapsed">
                                        <div class="widget-header">
                                            <h4 class="vdTitle">
                                                Instructions
                                            </h4>
                                            <div class="widget-toolbar ">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <span class="lead">
                                                    ${campaign.instructions}
                                                </span>
                                                <p class="align-left center no-margin-bottom">
                                                    Video Tags (add these to your video):${campaign.tags};
                                                </p>
                                                <p class="align-left center">
                                                    Video Length: ${creativeVideoLength} secs;
                                                </p>
                                            </div>
                                        </div>
                                    </div>

<!-- 
                                    <div class="title-with-lines title-left" style="padding: 6px 0px">Get App</div>
                                    <div class="row">
                                         <div class="col-sm-12">
                                            <div class="text-center">
                                                <img alt="Android app on Google Play"
                                                     src="<c:url value="/resources/assets/images/appstore.png"/>">
                                                <img alt="iPhone app on App Store" 
                                                	 src="<c:url value="/resources/assets/images/android.png"/>">
                                            </div>
                                         </div>
                                    </div>
 -->
									<security:authorize ifAnyGranted="ADMIN">
	                                    <div class="title-with-lines title-left" style="padding:6px 0px">Upload creative</div>
		                                <form method="POST" action="<c:url value="/secure/campaigns/campaign/uploadvideo"/>" accept-charset="UTF-8" class="separate-sections fill-up validatable" enctype="multipart/form-data">
		                                    <div class="text-center">
											    <input id="campaignId" type="hidden" name="campaignId" value="${campaign.id}"/><br />
											    <input id="videoFile" type="file" size="4" name="videoFile" value="Upload Video"/><br />									
		                                        <button class="btn btn-large btn-yellow align-middle">
		                                            <i class="icon-upload"></i>
		                                            Upload video
		                                        </button>
		                                    </div>
		                               </form>
	                               </security:authorize>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="title-with-lines" style="padding:25px">Creativity so far</div>
                    <div class="col-md-12">
                        <ul class="ace-thumbnails vd-public">
                            <c:if test="${empty campaign.creativeVideoList}">
                              	<span>No Videos Uploaded</span>
                            </c:if>                            
							<c:forEach items="${campaign.creativeVideoList}" var="media">                        
                                <c:if test="${media.mediaStatus == 'APPROVED' }">
	                               <li>
	                                    <a href="#myElement" data-rel="colorbox">
		                                        <img alt="${campaign.title}" src="${media.s3URL}.png" height="150" width="150" />
		                                        <div class="text">
		                                            <div class="inner">${campaign.title}</div>
		                                        </div>
	                                    </a>
	                                </li>
                                </c:if>
	 	                   </c:forEach>      
                        </ul>
                    </div>
                </div>
                <div class="title-with-lines" style="padding:25px">Social Search</div>
                <div id="instafeed"></div>
            </div>
        </div><!-- /.main-container-inner -->

        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>

    </div><!-- /.main-content -->

    <!-- ======== BASIC SCRIPTS ==========-->
    <!-- basic scripts -->

    <!--[if !IE]> -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-2.0.3.min.js"/>'>"+"<"+"/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script type="text/javascript">
        window.jQuery || document.write("<script src='<c:url value="/resources/assets/js/jquery-1.10.2.min.js"/>'>"+"<"+"/script>");
    </script>
    <![endif]-->

    <script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='<c:url value="/resources/assets/js/jquery.mobile.custom.min.js"/>'>"+"<"+"/script>");
    </script>
    <script src="<c:url value="/resources/assets/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/typeahead-bs2.min.js"/>"></script>

    <!-- page specific plugin scripts -->

    <!-- ace scripts -->

    <script src="<c:url value="/resources/assets/js/ace-elements.min.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/ace.min.js"/>"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function($) {
            $('.infocontainersmall').on({
                mouseenter: function() {
                    $(this).append("<div class='play'></div>");
                        },
                mouseleave: function() {
                    $("div").remove('.play');
                }
            });

        });
    </script>

</body>
</html>