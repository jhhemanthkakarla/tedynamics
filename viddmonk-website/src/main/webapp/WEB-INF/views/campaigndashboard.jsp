<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>viddmonk - campaign dashboard</title>
	    <%@include file="head-campaigndashboard.jsp" %>
	    <script type="text/javascript" src="<c:url value="/resources/js/instafeed.min.js"/>"></script>
		<script type="text/javascript">
		    var feed = new Instafeed({
		        get: 'tagged',
		        tagName: '${campaign.tags}',
		        clientId: '72b33f66c112462983a484fa3791335e'
		    });
		    feed.run();
		</script>
	</head>

<body>
    <c:set var="totalVideos" value="0" scope="page" />                        
    <jsp:include page="campaignpublictopnav.jsp">
    	<jsp:param name="tabitem" value="dashboard" />
    </jsp:include>
    <div class="main-container" id="main-container">
        <div class="page-content">
            <div class="col-md-12 center">
                <div class="infobox infobox-orange2  ">
                    <div class="infobox-icon">
                        <i class="icon-time"></i>
                    </div>
                    <div class="infobox-data">
                        <span class="infobox-data-number">${campaign.daysLeft}</span>
                        <div class="infobox-content">Days + ${campaign.hoursLeft} hrs</div>
                    </div>
                </div>
                <div class="infobox infobox-green  ">
                    <div class="infobox-icon">
                        <i class="icon-eye-open"></i>
                    </div>

                    <div class="infobox-data">
                        <span class="infobox-data-number">${campaign.numberOfViews}</span>
                        <div class="infobox-content">views</div>
                    </div>
                </div>
                <div class="infobox infobox-pink  ">
                    <div class="infobox-icon">
                        <i class="icon-film"></i>
                    </div>

                    <div class="infobox-data">
                        <c:set var="newCount" value="0" scope="page" />                        
                        <c:forEach items="${campaign.creativeImageList}" var="media">
                            <c:if test="${empty media.mediaStatus || (media.mediaStatus != 'APPROVED' && media.mediaStatus != 'REJECTED' && media.mediaStatus != 'LIKED') }">
								<c:set var="newCount" value="${newCount + 1}" scope="page"/>                                                    		
							</c:if>    
        				</c:forEach>
                        <span class="infobox-data-number">${newCount}</span>
                        <div class="infobox-content">new videos</div>
                    </div>
                </div>
                <div class="infobox infobox-blue  ">
                    <div class="infobox-icon">
                        <i class="icon-share"></i>
                    </div>

                    <div class="infobox-data">
                        <span class="infobox-data-number">11</span>
                        <div class="infobox-content">new shares</div>
                    </div>

                    <div class="badge badge-success">
                        +32%
                        <i class="icon-arrow-up"></i>
                    </div>
                </div>
            </div>
            <header>
                <h1 class="toupper align-center white">${campaign.title}</h1>
                <p class="lead align-center white no-margin-bottom">${campaign.description}</p>
                <p class="white text-center">${campaign.tags}</p>
                <div class="text-center">
                    <a href="<c:url value="/secure/campaigns/update/${campaign.id}"/>" class="btn btn-large btn-success">
                        Edit Campaign
                    </a>
                </div>
                <p>&nbsp;</p>
            </header>

            <div class="page-content-inner">
                <div class="text-center">
                <div class="btn-group">
                    <span class="lead"> Promote campaign
                        <a href="<c:url value="/public/campaigns/public/${campaign.id}"/>" class="btn btn-yellow">
                           View
                        </a>
                        <button class="btn">
							<a href="https://www.facebook.com/sharer.php?s=100&p[url]=http://viddmonk.com/s=100&p[title]=View Campaign&p[summary]=${fn:replace($campaign.tags,'#', '')}" 
								onclick="window.open('https://www.facebook.com/sharer.php?s=100&p[title]=View Campaign&p[summary]=${fn:replace(campaign.tags,'#', '')}&p[url]=http://viddmonk.com/','popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';" ><span style="color: white;"><i class="icon-facebook"></i></span></a>
                        </button>
                        <button class="btn" >                            
							<a href="https://twitter.com/share?url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;text=View Campaign &amp;hashtags=${fn:replace($campaign.tags,'#', '')}'" 
								onclick="window.open('https://twitter.com/share?url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&text=View Campaign&hashtags=${fn:replace(campaign.tags,'#', '')}','popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';"><span style="color: white;"><i class="icon-twitter"></i></span></a>
                        </button>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                        
                        <button class="btn">
							<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;title=View Campaign&amp;summary=${fn:replace(campaign.tags,'#', '')}&amp;source=http://www.viddmonk.com" 
							onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;title=View Campaign&amp;summary=${fn:replace(campaign.tags,'#', '')}&amp;source=http://www.viddmonk.com'),'popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';" target="_blank">
							<span style="color: white;"><i class="icon-linkedin"></i></span></a>                            
                        </button>
                        <button class="btn">
							<a href="https://plus.google.com/share?data-contentdeeplinkid=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&amp;content=View Campaign &amp;hashtags=${fn:replace($campaign.tags,'#', '')}'" 
								onclick="window.open('https://plus.google.com/share?url=http://www.viddmonk.com<c:url value="/public/campaigns/public/${campaign.id}"/>&content=View Campaign&hashtags=${fn:replace(campaign.tags,'#', '')}','popup','width=400,height=400,scrollbars=yes,resizable=yes,toolbar=no,directories=no,location=no,menubar=no,status=no,left=50,top=0'); return false" onmouseover="this.style.color='white';" onmouseout="this.style.color='white';"><span style="color: white;"><i class="icon-google-plus"></i></span></a>
                        </button>
                        <button class="btn">
                            <i class="icon-link"></i>
                        </button>
                    </span>
                </div>
                </div>
             	<h2 class="title-with-lines">Creativity so far</h2>                

                <div class="text-center"> <!-- SHOW THIS IN A LOOP -->
                    <c:forEach items="${campaign.creativeVideoList}" var="media" varStatus="mediaCount">
                       <div class="videocontainer align-center" >
                            <div class="videoplayer" id="video${mediaCount.index}" style="background-image: url('${media.s3URL}.png');"
                                 onclick=playvideo(video${mediaCount.index},"${media.s3URL}")>
                                 <div class="videoplayicon"></div>
                            </div>
                            <div class="videoinfo">                            
	                            <h4 class="vdTitle smaller">${campaign.title}</h4>
	                            <div class="btn btn-xs pull-left inline">
	                                <i class="icon-link "></i>
	                            </div>
	                            <label class="pull-right inline">
	                                <small class="muted">Approve: </small>
	                                <c:if test="${media.mediaStatus == 'APPROVED' }">
		                                <input id="id-pills-stacked${mediaCount.index}" type="checkbox" checked="checked" class="ace ace-switch ace-switch-5" onchange="statuschange('${campaign.id}','${media.id}', this);">
	                                </c:if>
	                                <c:if test="${media.mediaStatus != 'APPROVED' }">
		                                <input id="id-pills-stacked${mediaCount.index}" type="checkbox" class="ace ace-switch ace-switch-5" onchange="statuschange('${campaign.id}','${media.id}', this);">
	                                </c:if>
	                                <span class="lbl"></span>
	                            </label>
	                         </div>
                        </div>                    
                    </c:forEach>
               </div>
               <div class="title-with-lines" style="padding:25px">Social Search</div>
               <div id="instafeed"></div>
            </div>
        </div><!-- /.page content -->
        <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
            <i class="icon-double-angle-up icon-only bigger-110"></i>
        </a>
    </div><!-- /.main-container -->

	<%@include file="campaigndashboardfooter.jsp" %>

</body>

</html>