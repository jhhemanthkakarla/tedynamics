    <meta charset="utf-8"/>
    <title>viddMonk - campaign dashboard</title>
	<link href="<c:url value="/resources/assets/images/favicon.ico"/>" rel="icon" type="image/x-icon" />
    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- basic styles -->

    <link href="<c:url value="/resources/assets/css/bootstrap.min.css"/>" rel="stylesheet"/>    
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/jquery.msgbox.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/jquery.msgbox.min.css"/>"/>

    <!--[if IE 7]>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/font-awesome-ie7.min.css"/>" />
    <![endif]-->

    <!-- page specific plugin styles -->

    <!-- fonts -->

    <link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-fonts.css"/>"/>

    <!-- custom styles -->

    <link rel="stylesheet" href="<c:url value="/resources/assets/css/ace.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-rtl.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-skins.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/viddmonk.min.css"/>" />


    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/ace-ie.min.css"/>" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- settings handler -->

    <script src="<c:url value="/resources/assets/js/ace-extra.min.js"/>"></script>


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="<c:url value="/resources/assets/js/html5shiv.js"/>"></script>
    <script src="<c:url value="/resources/assets/js/respond.min.js"/>"></script>
    <![endif]-->
    <script type="text/javascript" src="<c:url value="/resources/assets/js/jwplayer/jwplayer.js"/>"></script>

	<script src="<c:url value="/resources/assets/js/videoplayer.js"/>"></script>
    