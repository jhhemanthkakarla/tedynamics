/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.view;

import java.io.Serializable;

/**
 * Response Error object
 * @author rvadlam
 *
 */
public class ResponseError implements Serializable {

	private static final long serialVersionUID = 1L;

	private ErrorType errorType;
	private String errorMessage;
	private String errorProperty;
	private String stacktrace;
	/**
	 * get errorType
	 * @return the errorType
	 *
	 */
	public ErrorType getErrorType() {
		return errorType;
	}
	/**
	 * set errorType
	 * @param errorType the errorType to set
	 *
	 */
	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}
	/**
	 * get errorMessage
	 * @return the errorMessage
	 *
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * set errorMessage
	 * @param errorMessage the errorMessage to set
	 *
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * get errorProperty
	 * @return the errorProperty
	 *
	 */
	public String getErrorProperty() {
		return errorProperty;
	}
	/**
	 * set errorProperty
	 * @param errorProperty the errorProperty to set
	 *
	 */
	public void setErrorProperty(String errorProperty) {
		this.errorProperty = errorProperty;
	}
	/**
	 * get stacktrace
	 * @return the stacktrace
	 *
	 */
	public String getStacktrace() {
		return stacktrace;
	}
	/**
	 * set stacktrace
	 * @param stacktrace the stacktrace to set
	 *
	 */
	public void setStacktrace(String stacktrace) {
		this.stacktrace = stacktrace;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResponseError [errorType=" + errorType + ", errorMessage="
				+ errorMessage + ", errorProperty=" + errorProperty
				+ ", stacktrace=" + stacktrace + "]";
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result
				+ ((errorProperty == null) ? 0 : errorProperty.hashCode());
		result = prime * result
				+ ((errorType == null) ? 0 : errorType.hashCode());
		result = prime * result
				+ ((stacktrace == null) ? 0 : stacktrace.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseError other = (ResponseError) obj;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		if (errorProperty == null) {
			if (other.errorProperty != null)
				return false;
		} else if (!errorProperty.equals(other.errorProperty))
			return false;
		if (errorType != other.errorType)
			return false;
		if (stacktrace == null) {
			if (other.stacktrace != null)
				return false;
		} else if (!stacktrace.equals(other.stacktrace))
			return false;
		return true;
	}
}
