package com.viddmonk.website.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.viddmonk.domain.AccountState;
import com.viddmonk.domain.Administrator;
import com.viddmonk.service.AdministratorService;

/**
 * Login Success Handler
 * @author Ranga Vadlamudi
 *
 */
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
	
	@Autowired
	private AdministratorService administratorService;

    /**
     * Called when a user has been successfully authenticated.
     *
     * @param request the request which caused the successful authentication
     * @param response the response
     * @param authentication the <tt>Authentication</tt> object which was created during the authentication process.
     */
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {    	
    	 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	 User user = (User)auth.getPrincipal();
    	 Administrator administrator = administratorService.findByUsername(user.getUsername());
     	 if( administrator != null && (administrator.getAccountState().equals(AccountState.Active) ||  administrator.getAccountState().equals(AccountState.PasswordReset) || administrator.getAccountState().equals(AccountState.UnVerfied) || administrator.getAccountState().equals(AccountState.InActive))) {
     		administrator.setNumberOfFailedAttempts(0);
     		administratorService.save(administrator);   		 
     		 if( administrator.getAccountState().equals(AccountState.UnVerfied) || administrator.getAccountState().equals(AccountState.PasswordReset) ) {
     			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/login/onetimepassword"));
     		 } else {
      			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/dashboard"));
     		 }
     	 }
    }
}