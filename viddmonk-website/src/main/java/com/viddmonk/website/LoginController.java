package com.viddmonk.website;

import java.security.Principal;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.viddmonk.domain.Administrator;
import com.viddmonk.service.AdministratorService;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("username")
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private AdministratorService administratorService;
	
	/**
	 * Display login page
	 */	
	@RequestMapping(value = {"/auth/login"}, method = RequestMethod.GET)
	public String home(Locale locale, Model model, Principal principal) {
		logger.info("Login Controller, auth page");
		return "signin";
	}
	
	/**
	 * Display signup page
	 * 
	 */	
	@RequestMapping(value = {"/signup"}, method = RequestMethod.GET)
	public String signup(Locale locale, Model model, Principal principal) {
		logger.info("Login Controller, signup page");
		return "signup";
	}
	
	/**
	 * Display signup page
	 * 
	 */	
	@RequestMapping(value = {"/login/verify"}, method = RequestMethod.GET)
	public String signin(Locale locale, Model model, Principal principal) {
		logger.info("Login Controller, signup page");
		return "signup";
	}
	
	/**
	 * register page
	 * 
	 */	
	@RequestMapping(value = {"/register"}, method = RequestMethod.POST)
	public String register(Locale locale, Model model,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "zip", required = true) String zip,
			@RequestParam(value = "email", required = true) String username) {
		logger.info("Login Controller, register page");
		Administrator administrator = new Administrator();
		administrator.setName(name);
		administrator.setEmail(username);
		administrator.setZipCode(zip);
		administratorService.registerAdministrator(administrator);
		return "registrationsuccess";
	}
	
	/**
	 * change password
	 * 
	 */	
	@RequestMapping(value = {"/login/changepassword"}, method = RequestMethod.POST)
	public String changepassword(Locale locale, Model model, Principal principal,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "confirmpassword", required = true) String confirmpassword) {
		logger.info("Login Controller, change password page");
		if( password.equals(confirmpassword) ) {
			Administrator administrator = new Administrator();
			administrator.setEmail(principal.getName());
			administrator.setUsername(principal.getName());
			administrator.setPassword(confirmpassword);
			Administrator newAdministrator = administratorService.changePassword(administrator);		
			if(newAdministrator == null ) {
				return "signin";
			}
		}
		return "dashboard";
	}
	
	/**
	 * Display password reset page
	 * 
	 */	
	@RequestMapping(value = {"/login/passwordreset"}, method = RequestMethod.GET)
	public String passwordreset(Locale locale, Model model) {
		logger.info("Login Controller, password reset page");
		return "passwordreset";
	}
	
	/**
	 * Display onetime password page
	 * 
	 */	
	@RequestMapping(value = {"/login/onetimepassword"}, method = RequestMethod.GET)
	public String onetimepassword(Locale locale, Model model) {
		logger.info("Login Controller, onetime password page");
		return "onetimepassword";
	}
	
	/**
	 * Complete password reset page
	 */	
	@RequestMapping(value = {"/login/passwordreset/complete"}, method = RequestMethod.POST)
	public String passwordresetcomplete(Locale locale, Model model,
			@RequestParam(value = "email", required = true) String username) {
		logger.info("Login Controller, auth page");
		Administrator administrator = new Administrator();
		administrator.setEmail(username);
		administrator.setUsername(username);
		administratorService.passwordReset(administrator);
		return "passwordresetsuccess";
	}
}