package com.viddmonk.service;

import static com.googlecode.javacv.cpp.opencv_core.cvCopy;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

import com.googlecode.javacv.FFmpegFrameGrabber;
import com.googlecode.javacv.FFmpegFrameRecorder;
import com.googlecode.javacv.Frame;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.IplROI;

public class TestRecorder {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			IplImage logoImg = cvLoadImage("/Users/rvadlam/Documents/VideoFiles/vdMonkLogo.png");
			IplROI roi = new IplROI();
			roi.xOffset(10);
			roi.yOffset(500);
			roi.width(30);
			roi.height(30);
			FFmpegFrameGrabber grabber1 = new FFmpegFrameGrabber("/Users/rvadlam/Documents/VideoFiles/VID4.mp4");
			grabber1.start();
			FFmpegFrameGrabber grabber2 = new FFmpegFrameGrabber("/Users/rvadlam/Documents/VideoFiles/VIDOriginal.mp4");
			grabber2.start();
			FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(
					"/Users/rvadlam/Documents/VideoFiles/PostVID1.mp4",
					grabber1.getImageWidth(), grabber1.getImageHeight(), grabber1.getAudioChannels());
			recorder.setFrameRate(grabber1.getFrameRate());
			recorder.setSampleRate(grabber1.getSampleRate());
			recorder.setVideoOption("movflags", "slow");
	        recorder.setVideoCodec(28);
	        System.out.println("Bit Rate: " + grabber1.getImageHeight()*grabber1.getImageWidth()*grabber1.getFrameRate());
	        recorder.setVideoBitrate(6612000);
	        
			
			
	        recorder.start();
			Frame frame;
			
			while (true) {		
				frame = grabber1.grabFrame();
				if( frame == null ) {
					break;
				}
				if( !frame.keyFrame ) {
					IplImage image = frame.image;
					if( image != null ) {
						IplImage image2 = image.roi(roi);
						cvCopy(logoImg,image2,null);					
					}					
				    try {
						recorder.record(image);			    		
					} catch (java.lang.Exception e) {
						e.printStackTrace();
					}					
				} else {
				    try {
						recorder.record(frame);			    		
					} catch (java.lang.Exception e) {
						e.printStackTrace();
					}										
				}
			}
			
			while (true) {		
				frame = grabber2.grabFrame();
				if( frame == null ) {
					break;
				}
				if( !frame.keyFrame ) {
					IplImage image = frame.image;
					if( image != null ) {
						IplImage image2 = image.roi(roi);
						cvCopy(logoImg,image2,null);					
					}					
				    try {
						recorder.record(image);			    		
					} catch (java.lang.Exception e) {
						e.printStackTrace();
					}					
				} else {
				    try {
						recorder.record(frame);			    		
					} catch (java.lang.Exception e) {
						e.printStackTrace();
					}										
				}
			}
			recorder.stop();
			grabber1.stop();
			grabber2.stop();
		} catch (com.googlecode.javacv.FrameRecorder.Exception e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
