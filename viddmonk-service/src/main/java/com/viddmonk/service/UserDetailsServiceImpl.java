/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.viddmonk.domain.User;
import com.viddmonk.dto.AuthenticationUserDetails;
import com.viddmonk.repository.UserRepository;

public class UserDetailsServiceImpl implements  UserDetailsService {

	private UserRepository userRepository;

    protected UserDetailsServiceImpl() {
    }

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User with login " + username + "  has not been found.");
        }
        return new AuthenticationUserDetails(user);
    }
}
