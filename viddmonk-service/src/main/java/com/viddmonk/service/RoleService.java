/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;

import java.util.List;

import com.viddmonk.domain.Role;

/**
 * Role Service
 * @author rvadlam
 *
 */
public interface RoleService {
		
	public Role findByRole(String role);
	
	public Role save(Role role);
	
	public List<Role> findAll();

}