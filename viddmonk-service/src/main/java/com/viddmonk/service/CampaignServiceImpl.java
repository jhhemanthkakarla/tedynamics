/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.Media;
import com.viddmonk.domain.MediaStatus;
import com.viddmonk.domain.MediaType;
import com.viddmonk.repository.CampaignRepository;
import com.viddmonk.repository.MediaRepository;

/**
 * Campaign Service
 * @author rvadlam
 *
 */
@Service("campaignService")
public class CampaignServiceImpl implements CampaignService {

	@Autowired
	private CampaignRepository campaignRepository;

	@Autowired
	private MediaRepository mediaRepository;
	
	private String AMAZON_BASE_URL = "https://s3.amazonaws.com/viddmonk";

	@Autowired
	private AmazonService amazonService;

	/**
	 * find by Id
	 * @param	campaignId	Campaign Id
	 * @return	Campaign
	 * 
	 */
	public Campaign findById(String campaignId) {
		return campaignRepository.findOne(campaignId);
	}	

	/**
	 * find by title
	 * @param	title	Title
	 * @return	Campaign
	 * 
	 */
	public Campaign findByTitle(String title) {
		return campaignRepository.findByTitle(title);
	}	
	
	/**
	 * save campaign
	 * @param	campaign	Campaign
	 * @return	Campaign
	 * 
	 */
	public Campaign save(Campaign campaign, MultipartFile videoFile) {
		if( videoFile != null ) {
			buildMedia(campaign, MediaType.VIDEO, videoFile);
		}		
		return campaignRepository.save(campaign);
	}
	
	/**
	 * save campaign
	 * @param	campaign	Campaign
	 * @return	Campaign
	 * 
	 */
	public Campaign save(Campaign campaign) {
		return campaignRepository.save(campaign);
	}

	/**
	 * build media
	 * @param event	Event
	 * @param mediaType	MediaType
	 * 
	 */
	private void buildMedia(Campaign campaign, MediaType mediaType, MultipartFile multipartFile) {
		Media media = new Media();
		media.setMediaStatus(MediaStatus.APPROVED);
		media.setFilename(multipartFile.getOriginalFilename());
		media.setFileSize(multipartFile.getSize());
		media.setMediaType(mediaType);
		media.setUploadedTime(new Date());
		long timestamp = Calendar.getInstance().getTimeInMillis();
		String filename = AMAZON_BASE_URL + "/" + mediaType.toString() + "/E_" + timestamp + media.getFilename();
		media.setS3URL(filename);
		try {			 
			amazonService.putAsset(mediaType.toString(), "E_" + timestamp + media.getFilename(), new ByteArrayInputStream(multipartFile.getBytes()));
			mediaRepository.save(media);
			if( mediaType.equals(MediaType.IMAGE) ) {
				campaign.getCreativeImageList().add(media);				
			} else if(mediaType.equals(MediaType.AUDIO) ) {
				campaign.getCreativeAudioList().add(media);
			} else if(mediaType.equals(MediaType.VIDEO) ) {
    	  		try {
					URL url = new URL(filename);
					File tmpFile = new File("/tmp/VideoFiles/");
					File myfile = Loader.extractResource(url,tmpFile, null, null);
	    	        final OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(myfile);
		            grabber.start();
		            IplImage img = grabber.grab();
		            if(img != null) {
		                String[] fileParts = media.getFilename().split("\\.");
		                if( fileParts != null && fileParts.length > 0 ) {
			                cvSaveImage("/tmp/VideoFiles/"+"E_" + timestamp + media.getFilename() + ".png", img);
			                File file = new File("/tmp/VideoFiles/"+"E_" + timestamp + media.getFilename() + ".png");
			                amazonService.putAsset(mediaType.toString(), "E_" + timestamp + media.getFilename() + ".png" , new ByteArrayInputStream(FileUtils.readFileToByteArray(file)));		                	
			                FileUtils.deleteQuietly(file);
		                }
		            }

    	  		} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				campaign.getCreativeVideoList().add(media);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}	
	/**
	 * get Campaigns list by brand
	 * @param	campaign	Campaign
	 * @return	Campaign
	 * 
	 */
	public Page<Campaign> getCampaignsByBrand(Brand brand, Pageable pageable) {
		return campaignRepository.findByBrand(brand, pageable);
	}
	
	/**
	 * find media by Id
	 * @param	mediaId	Media Id
	 * @return	Media
	 * 
	 */
	public Media findMediaById(String mediaId) {
		return mediaRepository.findOne(mediaId);
	}
	
	/**
	 * save media
	 * @param	media	Media
	 * @return	Media
	 * 
	 */
	public Media saveMedia(Media media) {
		return mediaRepository.save(media);
	}
	
	/**
	 * find all campaigns
	 * @return	Page<Campaign>
	 * 
	 */
	public Page<Campaign> findAll(Pageable pageable) {
		return campaignRepository.findAll(pageable);
	}
}