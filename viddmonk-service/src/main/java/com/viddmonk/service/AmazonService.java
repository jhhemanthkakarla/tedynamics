package com.viddmonk.service;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import com.viddmonk.dto.FileStream;

/**
 * Amazon Service
 * @author rvadlam
 *
 */
public interface AmazonService {
	
	public void putAsset(String path, String assetName, ByteArrayInputStream asset);

	public List<String> getAssetList(String path);

	public FileStream getAssetByName(String path, String name) throws FileNotFoundException;
}
