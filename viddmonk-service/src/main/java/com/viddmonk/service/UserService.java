/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;

import com.viddmonk.domain.User;

/**
 * User Service
 * @author rvadlam
 *
 */
public interface UserService {
		
	public User findByUsername(String username);
	
	public User registerUser(User user);
	
	public User passwordReset(User user);
	
	public User changePassword(User user);
	
	public User update(String username, String name, String mobileNumber, String zipCode, Boolean smsNotification, Boolean emailNotification);

}