/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viddmonk.domain.Role;
import com.viddmonk.repository.RoleRepository;

/**
 * Role Service
 * @author rvadlam
 *
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	/**
	 * find by role
	 * @param	role	Role
	 * @return	Role
	 * 
	 */
	public Role findByRole(String role) {
		return roleRepository.findByRole(role);
	}

	/**
	 * find by role
	 * @param	role	Role
	 * @return	Role
	 * 
	 */
	public List<Role> findAll() {
		return roleRepository.findAll();
	}
	
	/**
	 * save
	 * @param	role	Role
	 * 
	 */
	public Role save(Role role) {
		Role retrievedRole = findByRole(role.getRole());
		if( retrievedRole == null ) {
			return roleRepository.save(role);			
		} else {
			return roleRepository.save(retrievedRole);
		}
	}

}