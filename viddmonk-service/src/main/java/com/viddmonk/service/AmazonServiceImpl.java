package com.viddmonk.service;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.viddmonk.dto.FileStream;

/**
 * Amazon Service Impl
 * @author rvadlam
 *
 */
public class AmazonServiceImpl implements AmazonService {
	
private static final String FOLDER_SUFFIX = "/";
	
	private AmazonS3 s3;
	private String bucket;	
	
	/**
	 * put asset
	 * @param	path	Path
	 * @param	assetName	Asset Name
	 * @param	asset	InputStream
	 * 
	 */
	@Override
	public void putAsset(String path, String assetName, ByteArrayInputStream asset) {
		ObjectMetadata meta = new ObjectMetadata();
		if( asset != null ) {
			meta.setContentLength(asset.available());
			String s3Path = getS3Path(path) + assetName;
			s3.putObject(new PutObjectRequest(bucket, s3Path, asset, meta).withCannedAcl(CannedAccessControlList.PublicRead));			
		}
	}

	/**
	 * get asset by name
	 * @param	path	Path
	 * @param	name	Name
	 * @return	FileStream
	 * 
	 */
	@Override
	public FileStream getAssetByName(String path, String name)
			throws FileNotFoundException {		
		S3Object obj = s3.getObject(new GetObjectRequest(bucket, getS3Path(path) + name));
		FileStream result = new FileStream(obj.getObjectContent(), obj.getObjectMetadata().getContentLength());
		return result;
	}

	/**
	 * get asset list
	 * @param	path	Path
	 * @return	List<String>
	 * 
	 */
	@Override
	public List<String> getAssetList(String path) {
		List<String> result = new ArrayList<String>();		
		ObjectListing objList = s3.listObjects(bucket, getS3Path(path));
		for (S3ObjectSummary summary:objList.getObjectSummaries()) {
			if(! summary.getKey().endsWith(FOLDER_SUFFIX)){
				result.add(summary.getKey().substring(path.length()));
			}
		}
		return result;
	}

	/**
	 * get S3 Path
	 * @param path	Path
	 * @return	String
	 * 
	 */
	private String getS3Path(String path) {
		if (path.startsWith(FOLDER_SUFFIX)) {
			path = path.substring(1);
		}	
		
		return path + FOLDER_SUFFIX;
	}

	/**
	 * set bucket
	 * @param bucket
	 * 
	 */
	public void setBucket(String bucket){
		this.bucket = bucket;
	}
	
	/**
	 * constructor
	 * @param client	AmazonS3
	 * 
	 */
	public AmazonServiceImpl(AmazonS3 client) {
		s3 = client;
	}
}