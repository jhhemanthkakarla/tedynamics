/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.viddmonk.domain.Brand;
import com.viddmonk.repository.BrandRepository;

/**
 * Brand Service
 * @author rvadlam
 *
 */
@Service("brandService")
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandRepository brandRepository;

	/**
	 * find by id
	 * @param	id	Id
	 * @return	Brand
	 * 
	 */
	public Brand findById(String id) {
		return brandRepository.findById(id);
	}	

	/**
	 * find by name
	 * @param	name	Name
	 * @return	Brand
	 * 
	 */
	public Brand findByName(String name) {
		return brandRepository.findByName(name);
	}	
	
	/**
	 * save brand
	 * @param	brand	Brand
	 * @return	Brand
	 * 
	 */
	public Brand save(Brand brand) {
		return brandRepository.save(brand);
	}
	
	/**
	 * get brand list by parent brand
	 * @param	brand	Brand
	 * @return	Brand
	 * 
	 */
	public Page<Brand> getBrandsByParentBrand(Brand brand, Pageable pageable) {
		return brandRepository.findByParentBrand(brand, pageable);
	}

}