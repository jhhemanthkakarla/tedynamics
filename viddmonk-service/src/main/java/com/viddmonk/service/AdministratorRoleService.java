/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.util.List;

import com.viddmonk.domain.AdministratorRole;

/**
 * Administrator Role Service
 * @author rvadlam
 *
 */
public interface AdministratorRoleService {
		
	public AdministratorRole findByRole(String role);
	
	public AdministratorRole save(AdministratorRole administratorRole);
	
	public List<AdministratorRole> findAll();

}