/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viddmonk.domain.AdministratorRole;
import com.viddmonk.repository.AdministratorRoleRepository;

/**
 * Administrator Role Service
 * @author rvadlam
 *
 */
@Service("administratorRoleService")
public class AdministratorRoleServiceImpl implements AdministratorRoleService {

	@Autowired
	private AdministratorRoleRepository administratorRoleRepository;

	/**
	 * find by role
	 * @param	role	Role
	 * @return	AdministratorRole
	 * 
	 */
	public AdministratorRole findByRole(String role) {
		return administratorRoleRepository.findByRole(role);
	}

	/**
	 * find by role
	 * @param	role	Role
	 * @return	AdministratorRole
	 * 
	 */
	public List<AdministratorRole> findAll() {
		return administratorRoleRepository.findAll();
	}
	
	/**
	 * save
	 * @param	administratorRole	AdministratorRole
	 * 
	 */
	public AdministratorRole save(AdministratorRole administratorRole) {
		AdministratorRole retrievedAdministratorRole = findByRole(administratorRole.getRole());
		if( retrievedAdministratorRole == null ) {
			return administratorRoleRepository.save(administratorRole);			
		} else {
			return administratorRoleRepository.save(retrievedAdministratorRole);
		}
	}

}