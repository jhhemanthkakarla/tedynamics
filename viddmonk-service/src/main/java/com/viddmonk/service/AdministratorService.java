/**
 * Copyright Viddmonk 2013
 */
package com.viddmonk.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.viddmonk.domain.Administrator;
import com.viddmonk.domain.Brand;
import com.viddmonk.domain.Campaign;
import com.viddmonk.domain.CampaignAdministrator;

/**
 * Administrator Service
 * @author rvadlam
 *
 */
public interface AdministratorService {
		
	public Administrator findByUsername(String username);
	
	public Administrator registerAdministrator(Administrator administrator);
	
	public Administrator save(Administrator administrator);
	
	public Brand saveBrand(Brand brand, String username, MultipartFile imageFiles);

	public void saveCroppedImage(Campaign campaign);
	
	public Campaign saveCampaign(Campaign campaign, String brandId,
			String username, List<MultipartFile> imageFiles,
			List<MultipartFile> audioFiles, List<MultipartFile> videoFiles);
	
	public Administrator passwordReset(Administrator administrator);
	
	public Administrator changePassword(Administrator administrator);
	
	public List<Brand> myBrands(String email);
	
	public Page<CampaignAdministrator> myCampaigns(String email, Pageable pageable);

}