/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viddmonk.domain.PaymentCard;
import com.viddmonk.repository.PaymentCardRepository;

/**
 * Payment Service
 * @author rvadlam
 *
 */
@Service("paymentCardService")
public class PaymentCardServiceImpl implements PaymentCardService {

	@Autowired
	private PaymentCardRepository paymentCardRepository;

	/**
	 * find by id
	 * @param	id	id
	 * @return	PaymentCard
	 * 
	 */
	public PaymentCard findById(String id) {
		return paymentCardRepository.findById(id);
	}	

	/**
	 * find by nickname
	 * @param	name	nickname
	 * @return	PaymentCard
	 * 
	 */
	public PaymentCard findByNickName(String nickname) {
		return paymentCardRepository.findByNickname(nickname);
	}	
	
	/**
	 * save payment car
	 * @param	paymentcard	PaymentCard
	 * @return	PaymentCard
	 * 
	 */
	public PaymentCard save(PaymentCard paymentCard) {
		return paymentCardRepository.save(paymentCard);
	}

	/**
	 * find all
	 * @return	List<PaymentCard>
	 * 
	 */
	public List<PaymentCard> findAll() {
		return paymentCardRepository.findAll();
	}	
}