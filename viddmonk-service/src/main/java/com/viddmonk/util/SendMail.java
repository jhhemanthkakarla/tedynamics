package com.viddmonk.util;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * Send mail
 * @author rvadlam
 *
 */
public class SendMail
{
	@Autowired
	private JavaMailSenderImpl mailSender;
  
	/**
	 * send mail
	 * @param dear
	 * @param content
	 */
	public void sendMail(SimpleMailMessage simpleMailMessage) {
 
	   MimeMessage message = mailSender.createMimeMessage();
	   try {
		    MimeMessageHelper helper = new MimeMessageHelper(message, true);
		    helper.setFrom(mailSender.getUsername());
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(simpleMailMessage.getSubject());
			helper.setText(simpleMailMessage.getText()); 
	   } catch (MessagingException e) {
	    	throw new MailParseException(e);
	   }
	   mailSender.send(message);
   }
}