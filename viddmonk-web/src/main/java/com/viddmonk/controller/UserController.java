/**
 * Copyright Viddmonk 2013
 *  
 */
package com.viddmonk.controller;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.viddmonk.domain.User;
import com.viddmonk.service.UserService;

/**
 * User Controller
 * @author rvadlam
 *
 */
@Controller
@RequestMapping("/users/")
public class UserController extends BaseController {
	
	@Autowired
	private UserService userService;

    @RequestMapping(value="registerUser", method = { RequestMethod.POST })
    public @ResponseBody User registerUser(@RequestBody User user) throws ConstraintViolationException, Exception {
    	return userService.registerUser(user);
    }

    @RequestMapping(value="passwordReset", method = { RequestMethod.POST })
    public @ResponseBody User passwordReset(@RequestBody User user) throws ConstraintViolationException, Exception {
    	return userService.passwordReset(user);
    }

    @RequestMapping(value="changePassword", method = { RequestMethod.POST })
    public @ResponseBody User changePassword(@RequestBody User user) throws ConstraintViolationException, Exception {
    	return userService.changePassword(user);
    }
}