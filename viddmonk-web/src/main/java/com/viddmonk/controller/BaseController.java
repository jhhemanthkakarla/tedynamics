/**
 * Copyright Viddmonk 2013
 *  
 */
package com.viddmonk.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.viddmonk.util.ErrorUtil;
import com.viddmonk.view.ErrorType;
import com.viddmonk.view.ResponseError;

/**
 * Base Controller
 * 
 */
public abstract class BaseController {
	
	/**
	 * constraint violation exception
	 * @param constraintViolationException
	 * @return	ResponseEntity<?>
	 * 
	 */
    @ExceptionHandler
    public ResponseEntity<?> constraintViolationException(ConstraintViolationException constraintViolationException) {
		Map<String, List<ResponseError>> resultMap = new HashMap<String, List<ResponseError>>();
		if( constraintViolationException != null ) {
			List<ResponseError> responseErrorList = new ArrayList<ResponseError>();
			for(ConstraintViolation<?> constraintViolation : constraintViolationException.getConstraintViolations() ) {
				ResponseError responseError = new ResponseError();
				responseError.setErrorType(ErrorType.CONSTRAINT);
				responseError.setErrorMessage(constraintViolation.getMessage());
				responseError.setErrorProperty(constraintViolation.getPropertyPath().toString());
				responseErrorList.add(responseError);
			}			
			resultMap.put("errorList", responseErrorList);
		}
        return new ResponseEntity<Object>(ErrorUtil.buildConstraintError(constraintViolationException, ErrorType.CONSTRAINT), HttpStatus.BAD_REQUEST);
    }
}
