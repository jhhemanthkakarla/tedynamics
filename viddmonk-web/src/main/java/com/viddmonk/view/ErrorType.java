/**
 * Copyright Viddmonk 2013
 * 
 */
package com.viddmonk.view;

/**
 * Error Type View Object
 * @author rvadlam
 *
 */
public enum ErrorType {

	SEVERE("SEVERE"),
	DATABASE("DATABASE"),
	CONSTRAINT("CONSTRAINT");
	
	private String errorType;
	
	private ErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getErrorType() {
		return errorType;
	}
}